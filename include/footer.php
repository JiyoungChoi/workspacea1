 <footer id="footer">
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
    


        <div class="footer-top-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">User Navigation </h2>
                        <ul>
                            <li><a href="../pages/index.php">HOME</a></li>
                            <li><a href=about.php>About Us</a></li>
                            <li><a href="#">How to help</a></li>
                            <li><a href="#">News & Media</a></li>
                            <li><a href="#">Media Releases</a></li>
                            <li><a href="#">Gallery</a></li>
                            <li><a href="#">News & Media</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                            <li><a href="../pages/gp.php">Grief Process</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <ul>
                            <li><a href="./pages/for_vic.php">Victim Support</a></li>
                            <li><a href="#">Victims Rights</a></li>
                            <li><a href="../pages/gp.php">Grief Process</a></li>
                            <li><a href="#">Victims Rights</a></li>
                            <li><a href="../pages/media.php">Handling Media</a></li>
                            <li><a href="#">Legal System</a></li>
                            <li><a href="../pages/f_assist.php">Financial Assistance</a></li>
                            <li><a href="../pages/qhvsg_support.php">How QHVSG Helps</a></li>
                            <li><a href="../pages/funerals.php">Organising a Funeral</a></li>
                            <li><a href="../pages/mentally_ill.php">When the Offender is Mentally Ill</a></li>
                            <li><a href="../pages/support_agents.php">Support Agencies</a></li>
                            <li><a href="../pages/honouring.php">Honouring Loved Ones</a></li>
                  
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        
                        <ul>
                            <li><a href="#">Grieving Families</a></li>
                            
                            <li><a href="#">Grieving Friends</a></li>
                           
                            <li><a href="#">Get Involved</a></li>
                            <li><a href="#">Volunteer</a></li>
                            <li><a href="../pages/regichoice.php">Become A Member</a></li>
                             <li><a href="#">Make a Bequest</a></li>
                            <li><a href="#">Become a Corporate Partner</a></li>
                             <li><a href="#">Events</a></li>
                            <li><a href="#">Share Your Story</a></li>
                             <li><a href="../pages/shop.php">Shop</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-newsletter">
                        <h2 class="footer-wid-title">Newsletter</h2>
                        <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                        <div class="newsletter-form">
                            <form action="#">
                                <input type="email" placeholder="Type your email">
                                <input type="submit" value="Subscribe">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="copyright">
                        <p>&copy; QHVSG All Rights Reserved. </p>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="footer-card-icon">
                        <i class="fa fa-cc-discover"></i>
                        <i class="fa fa-cc-mastercard"></i>
                        <i class="fa fa-cc-paypal"></i>
                        <i class="fa fa-cc-visa"></i>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End footer bottom area -->
      </div>
    




  </footer>

  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  <script type="text/javascript" src="../js/jquery.inview.min.js"></script>
  <script type="text/javascript" src="../js/wow.min.js"></script>
  <script type="text/javascript" src="../js/mousescroll.js"></script>
  <script type="text/javascript" src="../js/smoothscroll.js"></script>
  <script type="text/javascript" src="../js/jquery.countTo.js"></script>
  <script type="text/javascript" src="../js/lightbox.min.js"></script>
  <script type="text/javascript" src="../js/main.js"></script>

</body>
</html>