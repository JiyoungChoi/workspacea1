<!DOCTYPE html>
<meta charset="ISO-8859-1">
<?php
session_start ();
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>QHVSG</title>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/animate.min.css" rel="stylesheet">
  <link href="../css/font-awesome.min.css" rel="stylesheet">
  <link href="../css/lightbox.css" rel="stylesheet">
  <link href="../css/main.css" rel="stylesheet">
  <link id="css-preset" href="../css/presets/preset1.css" rel="stylesheet">
  <link href="../css/responsive.css" rel="stylesheet">
   <link href="../css/style_nav.css" rel="stylesheet">

  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="../images/favicon.ico">
  <script type='text/javascript' src='http://code.jquery.com/jquery-1.7.1.js'></script>
</head><!--/head-->

<body>

<div class="header-area">
        <div class="container">
            <div class="row">


<ul class="list-inline " style="padding-left: 20px">

    <li class="list-inline-item pull-left text-info">24-HOUR FREECALL SUPPORT LINE: 1800 774 744</li>

              <?php
                    if (isset ( $_SESSION ['member'] ) || isset ( $_SESSION ['volunteer'] ) || isset ( $_SESSION ['admin']))
                    // check to see if a member or admin is logged in and, if so, display the logged in menu items
                    {
                      ?>

                  <li class="list-inline-item pull-right"> <a <?php if(isset ( $_SESSION ['volunteer'] )){
                      echo 'href="volanding.php"';
                  } 
                  elseif(isset ( $_SESSION ['admin'])){
                     echo 'href="https://qhvsg-jiyoung.c9users.io/pages/new_admin/admin_account.php"';
                      
                  }
                     else{
                       echo 'href="memberlanding.php"';
                  }
                  
                   ?> style="" >My Account</a></li>
                  <li class="list-inline-item pull-right"><a href="logout.php">Logout</a></li>
             <?php
                    }
                    else{
                      echo ' <li class="list-inline-item pull-right" >  <a href="../pages/login.php" > LOGIN/REGISTER </a></li>';
                    }

                     ?>





                    <!-- end navigation -->
                <?php if(isset ( $_SESSION ['admin'] )){
                                      
                                  } 
                                  else{ 
                                      echo '<li class="list-inline-item pull-right">  <a href="new_admin/admin_login.php" class=""> ADMIN </a></li>';
                                  } ?>
                        </ul>
                    </div>
               </div>
     <!-- End header area -->



  <div class="main-nav" style="margin:0px;">
  
        <div class="navbar-header" style="margin-left: 5%;">
          <button type="button" class="navbar-toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-slide-dropdown">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="logo" href="index.php" >
            <h1><span style="color:#fff; font-weight:900"; >QHVSG</h1>
          </a>
        </div>
        <div class="container">
        <div class="collapse navbar-collapse" id="bs-slide-dropdown">
          <ul class="nav navbar-nav navbar-right" style="padding-top:5px">
             <li class="dropdown">
          <a href="about.php" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="../pages/about.php" style="font-size:14px;">Who are we?</a></li>
            <li class="divider"></li>
            <li class=""><a href="../pages/history.php" style="font-size:14px;">The History of QHVSG</a></li>
            <li class="divider"></li>
            <li class=""><a href="../pages/charter.php" style="font-size:14px;">Our Charter</a></li>
            <li class="divider"></li>
            <li class=""><a href="../pages/education.php" style="font-size:14px;">Education</a></li>
            <li class="divider"></li>
            <li class=""><a href="../pages/team.php" style="font-size:14px;">Our Team</a></li>
            <li class="divider"></li>
            <li class=""><a href="../pages/sponsors.php" style="font-size:14px;">Our Sponsors</a></li>
          </ul>
          </li>
            <li class="dropdown">
          <a href="qhvsg_support.php" class="dropdown-toggle" data-toggle="dropdown">Victim Support<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a  style="font-size:14px;" href="../pages/qhvsg_support.php">How QHVSG can help</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="../pages/gp.php">Grief process</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="../pages/for_vic.php">Victims support</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="../pages/f_assist.php">Financial assistance</a></li>
            <li class="divider"></li>
            <li class=""><a style="font-size:14px;" href="../pages/funerals.php">Organising a funeral</a></li>
            <li class="divider"></li>
            <li class=""><a style="font-size:14px;" href="../pages/mentally_ill.php">When offender is metally ill</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="../pages/support_agents.php">Support agencies</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="../pages/honouring.php">Honouring loved ones</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="../pages/sup_meetings.php">QLD homicide support meetings</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="../pages/media.php">Handling media</a></li>
          </ul>
          </li>
            
            <li class="dropdown">
          <a href="qhvsg_support.php" class="dropdown-toggle" data-toggle="dropdown">How to help<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li class=""><a  style="font-size:14px;" href="">Grieving Families</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="">Grieving Friends</a></li>
            <li class="divider"></li>
            <li class=""><a  style="font-size:14px;" href="">Grieving Students</a></li>
            <li class="divider"></li>
            <li class=""><a style="font-size:14px;" href="">Grieving Employees and Colleagues</a></li>
            <li class="divider"></li>
            <li class=""><a style="font-size:14px;" href="">Government Service Providers</a></li>
        </ul>
          </li>
            
           


           
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a style="font-size:14px;" href="shop.php">Shop</a></li>
             <li class="divider"></li>
            <li><a style="font-size:14px;" href="m_shop_main.php">Memorabilia</a></li>
            <li class="divider"></li>
            <li><a style="font-size:14px;" href="cart.php">Cart</a></li>
        
            
          </ul>
        </li>

            <li class=""><a href="#">Gallery</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Chat<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a style="font-size:14px;" href="chat/practice.php">Private Chat</a></li>
            <li class="divider"></li>
            <li class=""><a style="font-size:14px;" href="private_chat/practice.php">StateWide Chat</a></li>
          </ul>
          </li>
          
          
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Social Media<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a style="font-size:14px;" href="event_test.php">Events</a></li>
            <li class="divider"></li>
            <li class=""><a style="font-size:14px;" href="feed_test.php">Blog</a></li>
          </ul>
          </li>
            
            
            <li class=""><a href="contact.php">Contact</a></li>
          </ul>
        </div>
      </div>
      </div>


