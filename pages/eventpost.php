

<?php
session_start ();
?>

<?php

$page = "eventpost";
include '../include/connect.php';


?>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>QHVSG</title>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/animate.min.css" rel="stylesheet">
  <link href="../css/font-awesome.min.css" rel="stylesheet">
  <link href="../css/lightbox.css" rel="stylesheet">
  <link href="../css/main.css" rel="stylesheet">
  <link id="css-preset" href="../css/presets/preset1.css" rel="stylesheet">
  <link href="../css/responsive.css" rel="stylesheet">
  <link href="../css/style_nav.css" rel="stylesheet">

  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->



  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="../images/favicon.ico">
  <script type='text/javascript' src='http://code.jquery.com/jquery-1.7.1.js'></script>
  <script src="https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyC-bEowsbt3KaVi_yTjtQxW8HUitDeUxCk"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCVau9-K84DQCFZb-ETxiiKveOfrR3HTc&callback=getLocation"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <script type="text/javascript" src="../js/address.js"></script>
</head><!--/head-->

<body>

<div class="header-area">
        <div class="container">
            <div class="row">


<ul class="list-inline " style="padding-left: 20px">

                         <li class="list-inline-item pull-left text-info">24-HOUR FREECALL SUPPORT LINE: 1800 774 744</li>

              <?php
                    if ((isset ( $_SESSION ['member'] )))
                    // check to see if a member or admin is logged in and, if so, display the logged in menu items
                    {
                      ?>

                  <li class="list-inline-item pull-right"> <a href="memberlanding.php" style="" >My Account</a></li>
                  <li class="list-inline-item pull-right"><a href="logout.php">Logout</a></li>
             <?php
                    }
                    else{
                      echo ' <li class="list-inline-item pull-right" >  <a href="../pages/login.php" > LOGIN/REGISTER </a></li>';
                    }

                     ?>





    <!-- end navigation -->

                          <li class="list-inline-item pull-right">  <a href='new_admin/admin_login.php' class=''> ADMIN </a></li>
                        </ul>
                    </div>
               </div>
     <!-- End header area -->



 <div class="main-nav" style="margin:0px;">
      <div class="container" >
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="logo" href="index.php" >
            <h1><span style="color:#fff; font-weight:900"; >QHVSG</h1>
          </a>
        </div>
        <div class="container">
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right" style="padding-top:10px">
            <li class=""><a href=about.php>About Us</a></li>
            <li class=""><a href="#">How to help</a></li>
            <li class=""><a href="shop.php">Shop</a></li>
            <li class=""><a href="#">Gallery</a></li>
            <li class=""><a href="chat/practice.php">Chat</a></li>
            <li class=""><a href="event.php">Blog</a></li>
            <li class=""><a href="contact.php">Contact</a></li>
          </ul>
        </div>
      </div>
      </div>

    </div><!--/#main-nav-->



<link href="../css/event.css" rel="stylesheet">
<?php
if(isset($_SESSION['member'])){
$memberID = $_SESSION ['member'];}
?>
<div class="container">
	<div class="row box box-pink">
		<div class="col-md-10 ">
			<hr>
			<h1>
				OUR <strong>EVENTS </strong>

			</h1>
			<hr>




 <?php
	$eventID = $_GET ['eventID'];
	$sql = "SELECT * FROM events WHERE eventID =$eventID";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
	$row = mysqli_fetch_array ( $result );
	$pageTitle = $row ['title'];
	$content = $row ['content'];
	$venue = $row ['venue'];
	$eventdate = $row ['eventdate'];
	$eventimg = $row ['eventimg'];

	?>
</div>

		<div class="col-md-8">
			<div class='img-responsive'>
			     <?php

echo "<img src='../images/blog/" . $eventimg . "'" . "style=' width:400px' height:400px;' background-size='cover'>";
								?>
			</div>
			<div class="col-md-pull-1">
				<span class="eventdatebtn"><?php
				$source = $eventdate;
				$date = new DateTime ( $source );
				echo $date->format ( 'd M' );
				echo "</button>";

				?>


			</div>
			<div class='col-md-push-3'>
				<h1><?php echo $pageTitle?></h1>
				<h4><?php echo $eventdate?></h4>

			</div>

<?php		echo '<a href="memberapply.php?eventID=' . $eventID . '" type="button"
 					class="btn btn-default btn-lg pull-right postbtn"> <span
 					class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span><strong>Paticipate Event</strong></a>';


?>


			<br>
			<div class='col-md-pull-8 card'>
				<h3><?php echo $content  ?></h3>
				<br />




			</div>
		</div>





	<div class="row col-md-4 ">
		<span class="eventdatebtn1">venue</span>
		<h3 id="address"><?php echo $venue
     ?></h3>




   <div id="map_canvas" style="height:50%; width:100%"></div>
   <script>

   </script>

	</div>
</div>

</div>
<?php
	include '../include/footer.php';
?>
