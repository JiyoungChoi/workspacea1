<?php 

include '../include/header.php';
?>

	<link rel="stylesheet" href="../css/main.css">
		<link rel="stylesheet" href="../css/calendar.css">
		<link rel="stylesheet" href="../css/calendar_full.css">
		<link rel="stylesheet" href="../css/calendar_compact.css">
		
	
		<script src="../js/languages/lang.js"></script>
		<script src="../js/jquery.min.js"></script>
		<script src="../js/calendar.js"></script>
		
	
		<link rel="stylesheet" href="../css/style.css">
		<link rel="stylesheet" href="../css/bootstrap-responsive.css">
	

	
		<div class="container">
			<div class="row box box-pink">
				<h1>QHVSG Events</h1>

			

				
				<div class="col-md-10">
			
					<!-- Call Full Events Calendar -->
					<div class="facebook-events-calendar full" data-view="list"></div>
				</div>
				
				<div class="col-md-2">
				
					
					<!-- Call Compact Events Calendar -->
					<div class="facebook-events-calendar compact"></div>
				</div>
			</div>
		</div>


<?php 
include '../include/footer.php';
?>