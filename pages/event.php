<?php
$page = "Event";
include '../include/connect.php';
include '../include/header.php'; // session_start(); included in header.php


?>
<link href="../css/event.css" rel="stylesheet">


<div class="container">

	<div class="row box box-yellow box_justfy">
		<div class="col-md-10">
			<hr>
			<h1 float="left">
				OUR <strong>NEWS & EVENTS

			</h1>
			<hr>
		</div>





  <?php
		$sql = "SELECT events.* FROM events ORDER BY events.eventdate DESC LIMIT 0,4 ";
		$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
		while ( $row = mysqli_fetch_array ( $result ) )

		{
			echo '<div class="col-md-6 col-sm-6" style="margin:30px 0px;" >';
			echo '<div class="eventcardtxt text-center  ">';
			echo '<h3>' . $row ['title'] . '</h3><hr>';
			echo '<h4>' . date ( "Y-m-d H:i", strtotime ( $row ['eventdate'] ) ) . '</h4>';

			echo '<p>' . $row ['venue'] . '</p>';
			echo '<a href="eventpost.php?eventID=' . $row ['eventID'] . '" type="button" class="btn btn-primary-outline event"><i>More details</i></a>';
			echo '</div>';

			echo "<img src='../images/blog/" . ($row ['eventimg']) . "'" . "style='width:250px; height:250px;' background-size='cover'>";
			echo '</div>';
		}
		?></div>
</div>
<?php

		include '../include/footer.php';
		?>



</div>
