<?php

require_once 'app/init.php';

if(isset($_POST['stripeToken'])) {
	$token = $_POST['stripeToken'];
	
	try {
		Stripe_Charge::create([
			"amount" => 2000,
			"currency" => "aud",
			"card" => $token,
			"description" => $user->email
		   ]);
		
		$db->query("
				 UPDATE users
				SET premium = 1
				WHERE id = {$user->id}
			");
		
	} catch(Stripe_CardError $e){
		//do sth error
		
	}
	
	header('Location: ../index.php');
	exit();
}