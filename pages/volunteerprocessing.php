<?php
session_start ();
include "../include/connect.php";
?>
<?php

$username = mysqli_real_escape_string ( $con, $_POST ['username'] ); // prevent SQL injection
$password = mysqli_real_escape_string ( $con, $_POST ['password'] );
$email = mysqli_real_escape_string ( $con, $_POST ['email'] );
$countpro = count($_POST['programs']);
if( $countpro == 2){
 $programs = 'OPCK and QHVSG';
}
            elseif($countpro == 1){
                 foreach($_POST['programs'] as $value){
                    $programs = $value;
                  }
            }
$gender = mysqli_real_escape_string ( $con, $_POST ['gender'] );
$firstname = mysqli_real_escape_string ( $con, $_POST ['firstname'] );
$lastname = mysqli_real_escape_string ( $con, $_POST ['lastname'] );
$contact = mysqli_real_escape_string ( $con, $_POST ['contact'] );
$address1 = mysqli_real_escape_string ( $con, $_POST ['address1'] );
$address2 = mysqli_real_escape_string ( $con, $_POST ['address2'] );
$city = mysqli_real_escape_string ( $con, $_POST ['city'] );
$postcode = mysqli_real_escape_string ( $con, $_POST ['postcode'] );
$state = mysqli_real_escape_string ( $con, $_POST ['state'] );
$confirmpassword = mysqli_real_escape_string ( $con, $_POST ['confirm_password'] );

$sqluser = "(SELECT username FROM member WHERE member.username='$username') UNION
(SELECT username FROM admin WHERE admin.username='$username') UNION (SELECT username FROM volunteer WHERE volunteer.username='$username')"; // check if the username is taken in the member table or the admin table as the username must be unique
$resultuser = mysqli_query ( $con, $sqluser ) or die ( mysqli_error ( $con ) ); // run the query
$numrow = mysqli_num_rows ( $resultuser ); // count how many rows are returned

$sqlemail = "(SELECT username FROM member WHERE member.email='$email') UNION
(SELECT username FROM admin WHERE admin.email='$email') UNION (SELECT username FROM volunteer WHERE volunteer.email='$email')"; // check if the username is taken in the member table or the admin table as the username must be unique
$resultemail = mysqli_query ( $con, $sqlemail ) or die ( mysqli_error ( $con ) ); // run the query
$numrowemail = mysqli_num_rows ( $resultemail ); // count how many rows are returned

$salt = md5 ( uniqid ( rand (), true ) ); // create a random salt value
$password = hash ( 'sha256', $password . $salt ); // generate the hashed password with the salt value



if ($firstname == '' || $lastname == '' || $gender == '' || $contact == '' || $address1 == '' || $programs == '' || $username == '' || $password == '' || $city == '' || $postcode == '' || $state == '') // check if all required fields have data

{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Please make sure all required fields are filled ')
         window.location.href='volregi.php'</SCRIPT>");
}

elseif (!preg_match('/^[a-zA-Z0-9]{8,}$/', $password)) // check if password is at least 8 characters long with at leasty 1 letter and number
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Password must be at least 8 charaters')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif ($_POST['confirm_password'] != $_POST['password']  ) // check if password is at least 8 characters long with at least 1 letter and number
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Password need to match')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif (!preg_match('/^[0-9]{0,10}$/', $contact)) // check if password is at least 8 characters long
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Contact number should not exceed 10 digits')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif (!preg_match('/^(\w+\s?){1,10}$/', $address1 )) // check if address complies with address format
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Address line 1 should comply with the standard format')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif ($address2 != '' && !preg_match('/^(\w+\s?){1,10}$/', $address2 )) // check if address complies with address format
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Address line 2 should comply with the standard format')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif (!preg_match('/^[A-Za-z ,.\'-]+$/', $firstname )) // check if address complies with address format
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('First names should comply with a standard format')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif (!preg_match('/^[A-Za-z ,.\'-]+$/', $lastname )) // check if address complies with address format
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Last names should comply with a standard format')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif (!preg_match('/^[0-9]{0,4}$/', $postcode )) // check if address complies with address format
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Postcode should not exceed 4 numbers')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif (!preg_match('/^[a-zA-Z0-9_-]{3,15}$/', $username )) // check if address complies with address format
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Invalid username format should be within 3 and 15 characters excluding special characters')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif (!preg_match('/^[A-Za-z ,.\'-]+$/', $city )) // check if address complies with address format
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('City/Suburb should not include numbers or special characters')
         window.location.href='volregi.php'
        </SCRIPT>");
}


elseif ($numrowuser > 0) // if count greater than 0
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('This username is already taken')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif ($numrowemail > 0) // if count greater than 0
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('This email is already taken. ')
         window.location.href='volregi.php'
        </SCRIPT>");
}

elseif (! filter_var ( $email, FILTER_VALIDATE_EMAIL )) // check if email is valid

{
	// if an error occurs intialise a session called 'error' with a
	
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Please enter a valid email address')
         window.location.href='volregi.php'
        </SCRIPT>");
}else {
	$sql = "INSERT INTO volunteer (username, password, salt, firstname, lastname, phone, email, gender, date, address_line1, address_line2, city, postcode, state, programs) VALUES ('$username', '$password', '$salt', '$firstname', '$lastname', '$contact', '$email', '$gender', now(), '$address1', '$address2', '$city', '$postcode', '$state', '$programs')";
	
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Sign up successful')
 
        window.location.href='login.php'
        </SCRIPT>");
}
?>
