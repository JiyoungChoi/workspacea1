<?php
$page = "cart";
include '../include/connect.php';
include '../include/header.php';
include '../include/functions.php'; // includes all the required PHP functions
?>

<link href="../css/m_shop.css" rel="stylesheet">

 
 <?php
	if (isset ( $_REQUEST ['command'] ) && $_REQUEST ['command'] == 'delete' && $_REQUEST ['pid'] > 0) {
		remove_product ( $_REQUEST ['pid'] );
	}  // if the $_REQUEST 'command' is 'clear' then unset the 'cart' session
else if (isset ( $_REQUEST ['command'] ) && $_REQUEST ['command'] == 'clear') {
		unset ( $_SESSION ['cart'] );
	}  // if the $_REQUEST 'command' is 'update' then update the cart by the specified quantity
else if (isset ( $_REQUEST ['command'] ) && $_REQUEST ['command'] == 'update') {
		$max = count ( $_SESSION ['cart'] );
		for($i = 0; $i < $max; $i ++) {
			$pid = $_SESSION ['cart'] [$i] ['productID'];
			$q = intval ( $_REQUEST ['product' . $pid] );
			if ($q > 0 && $q <= 999) {
				$_SESSION ['cart'] [$i] ['qty'] = $q;
			}
		}
	}
	?>
<!-- JavaScript that creates the 'delete', 'clear', and 'update' commands when the
corresponding button is clicked -->
<script>
 function del(pid){
 if(confirm('Do you really mean to delete this item?')){
 document.cart.pid.value=pid;
 document.cart.command.value='delete';
 document.cart.submit();
 }
 }
 function clear_cart(){
 if(confirm('This will empty your shopping cart, continue?')){
 document.cart.command.value='clear';
 document.cart.submit();
 }
 }
 function update_cart(){
 document.cart.command.value='update';
 document.cart.submit();
 }
</script>
<!-- the code could be improved by removing all the inline styling and placing
it in your external CSS file -->
<div class="container">

	<div class="row">
		<div class="box">
			<div class="shoppingcart">
				<form name="cart" method="post">
					<input type="hidden" name="pid" /> <input type="hidden"
						name="command" />
					<div class="cart">
						<div style="margin: 0px auto; width: 700px;">
							<div style="padding-bottom: 10px">
								<h2>Your Shopping Cart</h2>
								<p>
									<input type="button" value="Continue Shopping" class="pull-left" 
										onclick="window.location='m_shop_main.php'" />
								</p>
							</div>
							<table class="table">
								<thead>
									<tr>

										<th>Product Name</th>
										<th>Price</th>
										<th>Qty</th>
										<th>Subtotal</th>
										<th>Options</th>

									</tr>
								</thead>
								<tbody>
 <?php
	if (isset ( $_SESSION ['cart'] )) {
		
		$max = count ( $_SESSION ['cart'] );
		for($i = 0; $i < $max; $i ++) { // for each product in the cart get the following
			$pid = $_SESSION ['cart'] [$i] ['productID'];
			$q = $_SESSION ['cart'] [$i] ['qty'];
			$pname = get_product_name ( $pid );
			if ($q == 0)
				continue;
			?>
 <tr>
										<td><?php echo $pname?></td>
										<td>$ <?php
			echo (number_format ( (get_price ( $pid )), 2, '.', '' ))?></td>
										<td><input type="text" name="product<?=$pid?>" value="<?=$q?>"
											maxlength="3" size="2" /></td>
										<td>$ <?php
			echo (number_format ( (get_price ( $pid ) * $q), 2, '.', '' ))?></td>
										<td><a href="javascript:del(<?=$pid?>)">Remove</a></td>
									</tr>
 <?php
		}
		?>
 <tr>
										<td style="padding: 10px"><strong>Order Total: $ <?php
		
		$total = (number_format ( (get_order_total ()), 2, '.', '' ));
		echo "$" . $total;
		?></strong></td>
										<td style="padding: 10px" colspan="5" align="right"><input
											type="button" value="Clear Cart" onclick="clear_cart()"> <input
											type="button" value="Update Cart" onclick="update_cart()"> <input
											type="button" value="Checkout"
											onclick="window.location='shopconfirm.php'"></td>
									</tr>
 <?php
	} else {
		echo "<tr bgColor='#FFFFFF'><td>There are no items in your
shopping cart!</td>";
	}
	?>
 
							
							</table>
						</div>
					</div>
					<!-- end cart -->
				</form>
			</div>
		</div>
	</div>
</div>
<?php
include '../include/footer.php';
?>