<?php
// Load Stripe
require('lib/Stripe.php');


// Load configuration settings
$config = require('config.php');



if ($_POST) {
    Stripe::setApiKey($config['secret-key']);

    // POSTed Variables
    $token      = $_POST['stripeToken'];
    $first_name = $_POST['first-name'];
    $last_name  = $_POST['last-name'];
    $name       = $first_name . ' ' . $last_name;
    $address    = $_POST['address'] . "\n" . $_POST['city'] . ', ' . $_POST['state'] . ' ' . $_POST['zip'];
    $email      = $_POST['email'];
    $phone      = $_POST['phone'];
    $amount     = (float) $_POST['amount'];

    try {
        if ( ! isset($_POST['stripeToken']) ) {
            throw new Exception("The Stripe Token was not generated correctly");
        }

        // Charge the card
        $donation = Stripe_Charge::create(array(
            'card'        => $token,
            'description' => 'Donation by ' . $name . ' (' . $email . ')',
            'amount'      => $amount * 100,
            'currency'    => 'usd'
        ));

        // Build and send the email
        $headers = 'From: ' . $config['emaily-from'];
        $headers .= "\r\nBcc: " . $config['emaily-bcc'] . "\r\n\r\n";

        // Find and replace values
        $find    = array('%name%', '%amount%');
        $replace = array($name, '$' . $amount);

        $message = str_replace($find, $replace , $config['email-message']) . "\n\n";
        $message .= 'Amount: $' . $amount . "\n";
        $message .= 'Address: ' . $address . "\n";
        $message .= 'Phone: ' . $phone . "\n";
        $message .= 'Email: ' . $email . "\n";
        $message .= 'Date: ' . date('M j, Y, g:ia', $donation['created']) . "\n";
        $message .= 'Transaction ID: ' . $donation['id'] . "\n\n\n";

        $subject = $config['email-subject'];

        // Send it
        if ( !$config['test-mode'] ) {
            mail($email,$subject,$message,$headers);
        }

        // Forward to "Thank You" page
        header('Location: ' . $config['thank-you']);
        exit;


    } catch (Exception $e) {
        $error = $e->getMessage();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Stripe Donation Form</title>
    <link rel="stylesheet" type="text/css" href="style.css" media="all">
    <script type="text/javascript" src="https://js.stripe.com/v2"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript">
        Stripe.setPublishableKey('<?php echo $config['publishable-key'] ?>');
    </script>
    <script type="text/javascript" src="script.js"></script>
</head>
<body>
    <div class="wrapper">
        <h1>
          Donation
        </h1>


        <div class="messages">
            <!-- Error messages go here go here -->
        </div>

        <form action="donationprocessing.php" method="POST" class="donation-form">
            <fieldset>
                <legend>
                    Contact Information
                </legend>
                <div class="form-row form-first-name">
                    <label>First Name</label>
                    <input type="text" name="firstname" class="first-name text">
                </div>
                <div class="form-row form-last-name">
                    <label>Last Name</label>
                    <input type="text" name="lastname" class="last-name text">
                </div>
                <div class="form-row form-email">
                    <label>Email</label>
                    <input type="email" name="email" class="email text">
                </div>
                <div class="form-row form-phone">
                    <label>Phone</label>
                    <input type="text" name="phone" class="phone text">
                </div>


            </fieldset>

            <fieldset>
                <legend>
                    Your Generous Donation
                </legend>
                <div class="form-row form-amount">
                    <label><input type="radio" name="amount" class="set-amount" value="10"> $10</label>
                    <label><input type="radio" name="amount" class="set-amount" value="20"> $20</label>
                    <label><input type="radio" name="amount" class="set-amount" value="100"> $100</label>
                    <label><input type="radio" name="amount" class="set-amount" value="50"> $50</label>
                    <label><input type="radio" name="amount" class="set-amount" value="200"> $200</label>
                    <label><input type="radio" name="amount" class="set-amount" value="500"> $500</label>
                    <label><input type="radio" name="amount" class="other-amount" value="0"> Other:</label> <input type="text" class="amount text" disabled>
                </div>
                <div class="form-row form-number">
                    <label>Card Number</label>
                    <input type="text" autocomplete="off" class="card-number text" value="4242424242424242">
                </div>
                <div class="form-row form-cvc">
                    <label>CVC</label>
                    <input type="text" autocomplete="off" class="card-cvc text" value="123">
                </div>
                <div class="form-row form-expiry">
                    <label>Expiration Date</label>
                    <select class="card-expiry-month text">
                        <option value="01" selected>January</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                    <select class="card-expiry-year text">

                        <option value="2017">2017</option>
                        <option value="2018" selected>2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2019">2021</option>
                        <option value="2020">2022</option>
                    </select>
                </div>
                <div class="form-row form-submit">
                    <input type="submit" class="submit-button" value="Submit Donation">
                </div>
            </fieldset>
        </form>
    </div>

    <script>if (window.Stripe) $('.donation-form').show()</script>
    <noscript><p>JavaScript is required for the donation form.</p></noscript>

</body>
</html>
