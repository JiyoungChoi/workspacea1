<?php $page="volregi"; include '../include/connect.php'; include '../include/header.php'; // ?>


<?php if (isset ( $_SESSION [ 'member'] )) { echo ( "<SCRIPT LANGUAGE='JavaScript'>window.alert('You already Logged in')
        window.location.href='memberlanding.php'
        </SCRIPT>"); } else { ?>
<div class="container">
    <div class="row box">

        <hr>
        <h2>Become a Volunteer</h2>
        <hr>

        <?php // user messages if (isset ( $_SESSION [ 'error'] )) { echo '<div class="error">'; echo '<p>' . $_SESSION [ 'error'] . '</p>'; echo '</div>'; unset ( $_SESSION [ 'error'] ); } ?>
        <div class="volform_area" style="text-align:left; padding:15px;">
            <form action="volunteerprocessing.php" method="post" enctype="multipart/form-data">

                <p>We welcome applications from people who have lost someone through homicide and can contribute on a peer to peer level, but also those within the broader community who have empathy for what our members are going through and want to show support. We also welcome applications from individuals and or organisations who have a particular interest in the areas of or may be studying law and justice, counselling, events and fundraising and other related areas. </p>

                <p>Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.</p>
                <br><br>
                <div class="volform_fields_head">
                <label>What areas do you want to help in?</label>
                <br><br>
                <label>Name*</label>
                <br><br>
                <label>Gender*</label>
                <br><br><br>
                <label>Contact Number*</label>
                <br><br>
                <label>Mailing Address*</label>
                <br><br><br><br>
                <label>City/Suburb*</label>
                <br><br>
                <label>Postcode*</label>
                <br><br>
                <label>State*</label>
                <br><br>
                <label>Username*</label>
                <br><br>
                <label>Email*</label>
                <br><br>
                <label>Password*</label>
                <br><br>
                <label>Confirm Password*</label><br>
                <input type="button" onclick="viewpass()" value="Show Password" class="btn btn-warning downappl">
                </div>
                    <div class="volform_fields_input">
                            <input id="programs" type="checkbox" name="programs[]" value="QHVSG"> QHVSG 
                            <br>
                            <input id="programs" type="checkbox" name="programs[]" Value="OPCK"> One Punch Can Kill
                            <br>
                            <input id="name1" type="text" name="firstname" placeholder="First name" style="height:34px; padding:5px; width:45%" class="form-control" />
                            <input id="name2" type="text" name="lastname" placeholder="Last name"  style="height:34px; padding:5px; width:50%" class="form-control" />
                            <br>
                            <input type="radio" id="gender" name="gender" value="male"> Male<br>
                            <input type="radio" id="gender" name="gender" value="female"> Female<br>
                            <br>
                            <input id="contact" type="text" name="contact" placeholder="Contact Number" style="height:34px; padding:5px; width:95%;" class="form-control" />
                            <br>
                            <input id="address1" type="text" name="address1" placeholder="Address line 1" style="height:34px; padding:5px; width:95%;" class="form-control" />
                            <br>
                            <input id="address2" type="text" name="address2" placeholder="Address line 2" style="height:34px; padding:5px; width:95%;" class="form-control" />
                            <br>
                            <input id="city" type="text" name="city" placeholder="City/Suburb" style="height:34px; padding:5px; width:95%;" class="form-control" />
                            <br>
                            <input id="postcode" type="text" name="postcode" placeholder="Postcode" style="height:34px; padding:5px; width:95%;" class="form-control" />
                            <br><br>
                            <select name="state">
                                <option disabled selected="selected">Choose State</option>
                                <option id="state" name="state" value="QLD">QLD</option>
                                <option id="state" name="state" value="NSW">NSW</option>
                                <option id="state" name="state" value="VIC">VIC</option>
                                <option id="state" name="state" value="SA">SA</option>
                                <option id="state" name="state" value="WA">WA</option>
                                <option id="state" name="state" value="TAS">TAS</option>
                                <option id="state" name="state" value="NT">NT</option>
                            </select>
                            <br><br>
                            <input id="username" type="text" name="username" placeholder="Username" style="height:34px; padding:5px; width:95%;" class="form-control" />
                            <br>
                            <input id="email" type="email" class="form-control" name="email" style="height:34px; padding:5px; width:95%;" placeholder="Email Address" />
                            <br>
                            <input id="password" type="password" name="password" class="form-control" />
                            <br>
                            <input id="confirm_password" type="password" name="confirm_password" class="form-control" />
                            
                            <br />
                            <br>
                                <input type="submit" class="btn btn-primary" name="registration" class="form-control" value="Create New Account" onclick="validateregister()" />
                            </p>
            
                        </form>
                </div>
        </div>
    </div>
</div>



<?php }?>
<?php include '../include/footer.php'; ?>