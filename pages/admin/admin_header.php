<?php
session_start();
if(!isset($_SESSION['admin'])) {
    header("Location:admin_login.php");
  }
?>

<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="../../css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="../../css/admin_style.css"> <!-- Resource style -->
	<script src="../../js/admin/modernizr.js"></script> <!-- Modernizr -->
  	<link href="../../css/bootstrap.min.css" rel="stylesheet">
	<title></title>
</head>
<body>
	<header class="cd-main-header">
		<a href="#0" class="cd-logo"><h2>QHVSG</h2></a>
		
		<div class="cd-search is-hidden">
			<form action="#0">
				<input type="search" placeholder="Search...">
			</form>
		</div> <!-- cd-search -->

		<a href="#0" class="cd-nav-trigger">Menu<span></span></a>

		<nav class="cd-nav">
			<ul class="cd-top-nav">
				<li><a href="#0">Dashboard</a></li>
				<li><a href="../logout.php">Logout</a></li>
				<li class="has-children account">
					<a href="#0">
						Account
					</a>

					<ul>

						<li><a href="#0">My Account</a></li>
						<li><a href="admin_account_edit.php">Edit Account</a></li>
						<li><a href="../logout.php">Logout</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</header> <!-- .cd-main-header -->

	<main class="cd-main-content">
		<nav class="cd-side-nav">
			<ul>
		
			
				<li class="active"><a href="membermanage.php">member</a>
					
					
				</li>
				<li class="has-children notifications">
					<a href="#0">enquiry<span class="count">3</span></a>
					
					<ul>
						<li><a href="#0">All Notifications</a></li>
						<li><a href="#0">staff board</a></li>
						<li><a href="#0">Other</a></li>
					</ul>
				</li>

				<li class="has-children comments">
					<a href="enquirycheck.php">enquiry</a>
					
					<ul>
						<li><a href="#0">navi</a></li>
						<li><a href="#0">navi</a></li>
						<li><a href="#0">navi</a></li>
					</ul>
				</li>
			</ul>

			<ul>
				<li class="cd-label">navi</li>
				<li class="has-children bookmarks">
					<a href="#0">navi</a>
					
					<ul>
						<li><a href="#0">navi</a></li>
						<li><a href="#0">navi</a></li>
						<li><a href="#0">navi</a></li>
					</ul>
				</li>
				<li class="has-children images">
					<a href="#0">navi</a>
					
					<ul>
						<li><a href="#0">navi</a></li>
						<li><a href="#0">navi</a></li>
					</ul>
				</li>

				
			</ul>

			
		</nav>

		
	