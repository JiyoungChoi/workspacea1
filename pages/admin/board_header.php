<?php
session_start();
if(!isset($_SESSION['board'])) {
    header("Location:admin_login.php");
  }
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="../../css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="../../css/admin_style.css"> <!-- Resource style -->
	<script src="../../js/admin/modernizr.js"></script> <!-- Modernizr -->
  	<link href="../../css/bootstrap.min.css" rel="stylesheet">
	<title>BOARD</title>
</head>
<body>
	<header class="cd-main-header">
		<a href="#0" class="cd-logo"><h2>QHVSG</h2></a>
		
		<div class="cd-search is-hidden">
			<form action="#0">
				<input type="search" placeholder="Search...">
			</form>
		</div> <!-- cd-search -->

		<a href="#0" class="cd-nav-trigger">Menu<span></span></a>

		<nav class="cd-nav">
			<ul class="cd-top-nav">
				<li><a href="board_index.php">Dashboard</a></li>
				<li><a href="../logout.php">Logout</a></li>
				<li class="has-children account">
					<a href="#0">
						
						Account
					</a>

					<ul>

						<li><a href="#0">My Account</a></li>
						<li><a href="borad_account_edit.php">Edit Account</a></li>
						<li><a href="../logout.php">Logout</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</header> <!-- .cd-main-header -->

	<main class="cd-main-content">
		<nav class="cd-side-nav">
			<ul>
				<li class="cd-label">Main</li>
				<li class="has-children overview">
					<a href="#0">Mmeber</a>
					
					
				</li>
				<li class="has-children notifications active">
					<a href="#0">Notifications<span class="count">3</span></a>
					
					<ul>
						<li><a href="#0">All Notifications</a></li>
						
					</ul>
				</li>

				
			</ul>

			<ul>
				<li class="cd-label">Secondary</li>
				<li class="has-children bookmarks">
					<a href="#0">Manage Staffs</a>
					
					
				</li>
				

				<li class="has-children users">
					<a href="#0">Users</a>
					
					<ul>
						<li><a href="#0">All Users</a></li>
						<li><a href="#0">Edit User</a></li>
						<li><a href="#0">Add User</a></li>
					</ul>
				</li>
				
		</nav>

		
	