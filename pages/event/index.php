

<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/calendar.css">
		<link rel="stylesheet" href="assets/css/calendar_full.css">
		<link rel="stylesheet" href="assets/css/calendar_compact.css">
		
		<!-- Include events calendar language file -->
		<script src="assets/languages/lang.js"></script>
	
		<!-- Include events calendar js file -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/calendar.js"></script>
		
		<!-- Example css -->
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/bootstrap-responsive.css">
	

	
		<div class="container">
			<div class="row box box-pink">
				<h1>QHVSG Events</h1>

			

				
				<div class="col-md-8">
			
					<!-- Call Full Events Calendar -->
					<div class="facebook-events-calendar full" data-view="list"></div>
				</div>
				
				<div class="col-md-4">
				
					
					<!-- Call Compact Events Calendar -->
					<div class="facebook-events-calendar compact"></div>
				</div>
			</div>
		</div>


