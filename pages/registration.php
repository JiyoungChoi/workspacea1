<?php $page="Registration" ; include '../include/connect.php'; include '../include/header.php'; // ?>
<script type="text/javascript" src="../js/validation.js"></script>

<?php if (isset ( $_SESSION [ 'member'] )) { echo ( "<SCRIPT LANGUAGE='JavaScript'>window.alert('You already Logged in')
        window.location.href='memberlanding.php'
        </SCRIPT>"); } else { ?>
<div class="container">
    <div class="row box">

        <hr>
        <h2>Become a member</h2>
        <hr>

        <?php // user messages if (isset ( $_SESSION [ 'error'] )) { echo '<div class="error">'; echo '<p>' . $_SESSION [ 'error'] . '</p>'; echo '</div>'; unset ( $_SESSION [ 'error'] ); } ?>
        <div class="col-md-6 ">
            <form action="registrationprocessing.php" method="post" enctype="multipart/form-data">

                <p>Membership to QHVSG is by application only and subject to review and approval by QHVSG Board of Management. To submit your application please complete all details below and submit this form together with your payment. Any application deemed unacceptable will have any deposit refunded immediately.</p>

                <p>Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.</p>

                <label>Username*</label>
                <input id="username" type="text" name="username" style="height:34px; padding:5px;" class="form-control" />
                </br>
                <label>Email*</label>
                <input id="email" type="email" style="height:34px; padding:5px; width:95%;" class="form-control" name="email" />
                <br/>
                <label>Password*</label>
                <input id="password" type="password" name="password" class="form-control" />
                <br />
                <label>Confirm Password*</label>
                <input id="confirm_password" type="password" name="confirm_password" class="form-control" />
                <input type="button" onclick="viewpass()" value="Show Password" class="btn btn-warning downappl">
                <br />
                <label>upload application form</label>
                <input class="form-control" type="file" name="file" style="height:50px; padding:5px;" />
                <br>
                <p></p>
                </br>
                </br>
                <p>
                    <input type="submit" class="btn btn-primary" name="registration" class="form-control" value="Create New Account" onclick="validateregister()" />
                </p>
                <p>*If you send your membership application form by mail then we will send a temporary password to your e-mail address. If you don't receive an email within 24 hours please don't hesitate to contact us.</p>
            </form>

        </div>
        <div class="col-md-3 block-center">
            <a href="../file/2017-Membership-Application.pdf" download>
                <button class="btn btn-warning downappl">
                    Download Membership application form
                </button>
            </a>
        </div>
    </div>
</div>



<?php }?>
<?php include '../include/footer.php'; ?>