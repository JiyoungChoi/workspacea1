<?php
$page = "product";
include '../include/connect.php';
include '../include/header.php';
include '../include/functions.php'; // includes all the required PHP functions
?>

<body>
	<div class="container">
		<div class="row box box-mint">

			<div class="col-md-12">
				
				<h2 class="text-left">Product Information</h2>
				<hr>
			</div>
<?php
// if the $_REQUEST 'command' is 'add' than call the PHP addtocart function
// $_REQUEST is similar to $_GET and $_POST, but it contains the contents of both
if (isset ( $_REQUEST ['command'] ) && $_REQUEST ['command'] == 'add' && $_REQUEST ['productID'] > 0) {
	$pid = $_REQUEST ['productID'];
	addtocart ( $pid, 1 );
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Succesfully added to cart')
        window.location.href='cart.php'
        </SCRIPT>");
	//header("location:cart.php");
	exit ();
}
?>

<!-- JavaScript that creates the 'add' command if the 'Add to Cart' button is clicked -->
			<script>
 function addtocart(pid){
 document.form1.productID.value=pid;
 document.form1.command.value='add';
 document.form1.submit();
 }
</script>
			<!-- form with hidden fields to send productID and the $_REQUEST 'command' to the
next page -->

			<form name="form1">
				<input type="hidden" name="productID" /> <input type="hidden"
					name="command" />
			</form>

			<div class="col-md-12">

<?php
$sql = "SELECT * FROM product"; // sql query
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) // display the results
	
	?>



<?php

$productID = mysqli_real_escape_string ( $con, $_GET ['productID'] );
$sql = "SELECT * FROM product WHERE productID =$productID";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
while ( $row = mysqli_fetch_array ( $result ) ) {
	echo "<div class='col-md-6 col-md-offset-1'>";
	echo "<img src='../images/shop/" . ($row ['productImage']) . "'" . "  class ='img-responsive' alt='product'" . " width='300px;'/>"; // display the image stored inside the images subfolder in another subfolder named shop (in your database store the image name e.g., image.jpg in the image column of your product table) >
	echo "</div >";
	echo "<div class='col-md-4'>";
	echo "<div class='row'>";
	echo "<h3>" . $row ['productName'] . "</h3><hr>";
	echo "<h4 class='text-left'>" . $row ['productDescription'] . "</h4><br><hr>";
	echo "<h4 class='text-right'><strong>Price: $" . $row ['productPrice'] . "</strong></h4></div><hr>";
	echo "<div class='row'><input type='button' class='btn btn-danger pull-right ' value='Add to Cart' onclick='addtocart(" . $row['productID' ] . ")' /></div>"; // 'Add to Cart' button
			echo "</div>";
	
	echo "</div>";
}

?>

</div>

			</div>
		</div>
	</div>



<?php
include "../include/footer.php";
?>