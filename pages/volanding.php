<?php
include '../include/header.php';
include "../include/connect.php";
$page = "volunteer landing";

if(!isset($_SESSION['volunteer'])) {
    header("Location:login.php");
  }?>
  

  
  <style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: black;
  text-align: center;
  font-size: 16px;
  padding: 12px;
  width: 120px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button1 {
  display: inline-block;
  border-radius: 4px;
  background-color: red;
  border: none;
  color: black;
  text-align: center;
  font-size: 16px;
  padding: 12px;
  width: 120px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}
.button .button1 span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button .button1 span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button .button1:hover span {
  padding-right: 25px;
}

.button .button1:hover span:after {
  opacity: 1;
  right: 0;
}
</style>
<!-- PAGE HEADER -->
    <!-- PAGE HEADER -->
    <div class="page_header">
        <div class="page_header_parallax">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3><span>Services</span>My <br>Account (volunteer)</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="bcrumb-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="bcrumbs">
                            <li><a href="../index.php"><i class="fa fa-home"></i> Home</a></li>
                            <li>My account</li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<div class="container body_con" style="margin-top:20PX;">
    <div class="row">
        <div class="col-md-12">
            <div class="tab" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Account</a></li>
                    <li role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">Notice</a></li>
                    <li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Order</a></li>
                    <li role="presentation"><a href="#Section4" aria-controls="settings" role="tab" data-toggle="tab">Schedule</a></li>
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="Section1">
                       <?php

$volunteeruser = $_SESSION ['volunteer'];
$sql = "SELECT * FROM volunteer where username = '$volunteeruser'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) 
{
    $fname = $row ['firstname'];
    $lname = $row ['lastname'];
    $email = $row ['email'];
    $mobile = $row ['phone'];
    $username = $row['username'];
    $addressline1 = $row['address_line1'];
    $isvolunteer = $row['isvolunteer'];
    
    
    echo "<ul class='list-group'>";
    echo "<li class='list-group-item'><h4> NAME: <span class='text-danger'>$fname $lname </h4></li>";
    echo "<li class='list-group-item'><h4> E-Mail: <span class='text-danger'>$email </h4></li>";
    echo "<li class='list-group-item'><h4> PHONE :<span class='text-danger'>$mobile </h4> </li></ul>";
                  
                    ?>


                            
                                
                                <h2>UPDATING ACCOUNT</h2>
                                            <hr>
                                            <p>Update your account details.</p>

                                            <form action="accountprocessing.php" method="post">
                                                <div class="form-group">
                                                    <label>Username*</label> <input type="text"
                                                        class="form-control" name="username" 
                                                        value="<?php echo $username; ?>" readonly /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>First Name*</label> <input type="text"
                                                        class="form-control" class="form-control" name="firstname"
                                                        required value="<?php echo $row['firstname']; ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Last Name*</label> <input type="text"
                                                        class="form-control" name="lastname" required
                                                        value="<?php echo $row['lastname'] ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Address line 1</label> <input type="text"
                                                        class="form-control" name="address_line1"
                                                        value="<?php echo $row ['address_line1']?>" /><br />

                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Address Line 2</label> <input type="text"
                                                        class="form-control" name="address_line2"
                                                        value="<?php echo $row ['address_line2']?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>City</label> <input type="text"
                                                        class="form-control" name="city"
                                                        value="<?php echo $row ['city']?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>State</label>
<?php

$tableName = 'volunteer';
$colState = 'state';
function getEnumState($tableName, $colState) {
    global $con;
    $sql = "SHOW COLUMNS FROM $tableName WHERE field='$colState'";
    // retrieve enum column
    $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
    $row = mysqli_fetch_array ( $result );
    $type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
    $enumValues = substr ( $type, 0, - 1 );
    $enumExplode = explode ( ',', $enumValues );
    return $enumExplode;
}
$enumValues = getEnumState ( 'member', 'state' );
echo '<select name="state" class="form-control">';

if ((is_null ( $row ['state'] )) || (empty ( $row ['state'] ))) // if the state field is NULL or empty
{
    echo "<option value=''>Please select</option>";
} else {
    echo "<option value=" . $row ['state'] . ">" . $row ['state'] . "</option>"; // display the selected enum value
}

foreach ( $enumValues as $value ) {
    echo '<option value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>'; // remove the quotes from the enum values
}
echo '</select>';
?>

<p>&nbsp;</p>
                                                </div>

                                                <div class="form-group">
                                                    <label>Postcode*</label> <input type="text"
                                                        class="form-control" name="postcode" required
                                                        value="<?php echo $row['postcode'] ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Phone</label> <input type="text" name="phone"
                                                        class="form-control"
                                                        value="<?php
                                                        
                                                        echo $row ['phone']?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Email*</label><br> <input type="email" name="email"
                                                        class="form-control" required
                                                        value="<?php echo $row['email'] ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Gender*</label>
<?php
// generate drop-down list for gender using enum data type and values from database
$tableName = 'volunteer';
$colGender = 'gender';
function getEnumGender($tableName, $colGender) {
    global $con; // enable database connection in the function
    $sql = "SHOW COLUMNS FROM $tableName WHERE field='$colGender'";
    // retrieve enum column
    $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
    // run the query
    $row = mysqli_fetch_array ( $result ); // store the results in a variable named $row
    $type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
    $enumValues = substr ( $type, 0, - 1 ); // return the enum string
    $enumExplode = explode ( ',', $enumValues ); // split the enum string into individual values
    return $enumExplode; // return all the enum individual values
}
$enumValues = getEnumGender ( 'volunteer', 'gender' );
echo '<select name="gender" class="form-control">';

echo "<option value=" . $row ['gender'] . ">" . $row ['gender'] . "</option>"; // display the selected enum value

foreach ( $enumValues as $value ) {
    echo '<option value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>';
}
echo '</select>';
?></div>

                                                <input type="hidden" name="volunteeruser"
                                                    value="<?php echo $volunteeruser; ?>"> <input type="submit"
                                                    name="accountupdate" value="Update Account" class="btn-submit" />
                                            </form>

                                      
</br>
        <h3>Update Password</h3>
                                        <p>Passwords must have a minimum of 8 characters.</p>
                                        <form action="accountpasswordprocessing.php" method="post">
                                            <label>New Password*</label> <input type="password"
                                                name="password" pattern=".{8,}"
                                                title="Password must be 8 characters or more" required /><br />
                                            <input type="hidden" name="memberID"
                                                value="<?php echo $volunteeruser; ?>">
                                            <p>
                                            
                                            
                                            <div class="form-group">
                                                <input type="submit" name="passwordupdate"
                                                    value="Update Password" class="btn-submit" />
                                            </div>
                                            
                                        </form>

</p></form></div>

                    <div role="tabpanel" class="tab-pane" id="Section2">
                       
                        <p>
                           <?php 
    
	   //                     $sql = "SELECT * FROM member ";
				// 			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
				// 			$row_count = mysqli_num_rows ( $result );
							
				// 			while ( $row = mysqli_fetch_array ( $result ) ) {
				// 			    if ($row ['isinvited'] == 1) {
							     
				// 			    $decline='	
				// 			<form method="post" action="run.php">    
				// 			    <input style="position: absolute; visibility: hidden;" type="text" name="username" value="'.$username.'"/>
			 //                   <input  style="text-decoration:none; color:white;" type="submit" name="invbtn" class="button1" value="Decline"/>
		  //                  </form>';
				// 			}

				// 			}
	echo "<h4>Notice</h4>";
	echo "<ul class='list-group'>";

	if($isvolunteer == 0) {
		echo "<li class='list-group-item'><span class='text-danger'><h5>*Your application is still being processed</li>";
	}
	if ($isvolunteer == 1) {
		echo "<li class='list-group-item'><span class='text-danger'><h5>*congratulations!! You are now a registered volunteer of QHVSG.</li>";
	}
    if ($isinvited == '1') {
	echo "<li class='list-group-item'>
		<a href='chat/practice.php' style='text-decoration:none; color:black;'><span class='text-danger'><h5>You are invited into this Private chatroom <br><button class='button' style='vertical-align:middle; margin-left:5px;'><span> Accept</span></button></a>
			<form method='post' action='run.php'>    
			<input style='position: absolute; visibility: hidden;' type='text' name='username' value='".$username."'/>
			 <input  style='text-decoration:none; color:white;' type='submit' name='invbtn' class='button1' value='Decline'/>
		  </form>
		</li>";
		
    }
    else  {
       echo "<li class='list-group-item'><span class='text-danger'><h5>*You have no chatroom requests right now.</li>";	
    }


?>
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Section3">
                        <h3>ORDER</h3>
                        <p>
                            3 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean varius quis ipsum a rutrum. Donec quis consequat sem. Donec gravida nisi quis nibh vestibulum ullamcorper. Suspendisse turpis sapien, lobortis vel.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Section4">
                        <h3>SCHEDULE</h3>
                        <p>
                            4 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas bibendum ante in velit faucibus, a efficitur nisl vestibulum. Sed a nisi eget tellus eleifend mattis. Nam vulputate leo in posuere imperdiet. Mauris a mollis nulla. Nulla euismod sed neque eget lacinia. Donec at lacus et arcu consectetur aliquet ac sed tellus. Donec non dapibus turpis
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
</br>
	  
			

		<?php } ?>
				
				
	




<?php require '../include/footer.php';?>