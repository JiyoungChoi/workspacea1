<?php
include '../include/connect.php';
include '../include/header.php';

?>


<div class="container">
	<div class="row box box-blue">
		<h2>Donation</h2>
		<hr>
    <p>
    Donations are vital to QHVSG’s ongoing operations and increasing members support services. All donations, regardless of amount, are graciously appreciated, and every dollar donated to QHVSG goes towards ensuring individuals and families devastated by the tragedy of homicide, receive the very best level of specialist care and support. Please remember QHVSG is a ‘DGR’ registered not-for-profit charity and all donations of $2 or more are tax deductible.
    </p>
    <div class="row donate">



            <div class="col-md-12 ">
				<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="KVV8JGDLRD8SW">
<input type="image" src="https://www.sandbox.paypal.com/en_AU/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_AU/i/scr/pixel.gif" width="1" height="1">
</form>


</div>

						<br>
                
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading ">Electronic Funds Transfer</div>
                    <div class="panel-body text-left">
                      <dl>
                        <dt>
                      BOQ</dt>
<dd>BSB:124-009</dd>
<dd>Account:20132624</dd>
<dd>Reference: Donation – FIRST NAME SURNAME</dd>
                    </div>
                </div>


                <div class="panel panel-success">
                    <div class="panel-heading ">Cheque</div>
                    <div class="panel-body text-left">
                        Please make cheques payable to ‘QHVSG’ and return to:
                      <dt>Finance Officer</dt>


<dt>QHVSG</dt>
<dt>PO Box 292</dt>
<dt>Lutwyche QLD 4030</dt>
                    </div>
                </div>

        </div></div>

  </div>

</form>

</div></div>
<?php

include '../include/footer.php';
?>
