<?php
define('INCLUDE_CHECK',1);
require "conn.php";
include '../include/header.php';

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
		<title>QHVSG Shopping Cart</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
 
 <style>
 

.col-30 {
  overflow: hidden;
  position: fixed;
  width: 25%;
  right: 0;
  margin-right: 35px;

}

.col-30 a {
  display: block;
  text-align: center;
  text-decoration: none;

}

.content{
	border-radius: 25px;
    border: 2px solid;
    padding: 20px; 
}

</style>
</head>
	<body>

	<div class="shop">
		<div class="col-100">
			<div class="col-70">
				<h1>Our Products</h1>
				<hr>
				<h3>All items:</h3>
				<?php
				$result = mysql_query("SELECT * FROM stock");
				while($row=mysql_fetch_assoc($result))
				{
					echo '
				<div class="item">
					<img src="images/'.$row['img'].'" alt="'.htmlspecialchars($row['name']).'"/>
					 <br/>'.$row['name'].'<br/>
					 $ '.$row['price'].'<br/>
					   
				</div>';
				}
				?>
			</div>
			<div class="col-30">
					<h1 class="label-txt">My Shopping Cart</h1>
				<div class="content drop-box" style="height:auto;">
					<span><strong>Drop your Products Here</strong></span>
					<form name="checkoutForm" method="post" action="checkout.php">
						<div id="item-list"></div>
					</form>
					<div id="total"></div>
					<a href="" onclick="document.forms.checkoutForm.submit(); return false;" class="button" style="width:30%;">Checkout</a>
				</div>
			</div>
		</div>
	</div>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
		<script type="text/javascript" src="https://cdn.css.net/files/jquery.simpletip/1.3.1/jquery.simpletip-1.3.1.pack.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</body>
</html>

