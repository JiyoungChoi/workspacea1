<?php


include '../include/connect.php';
include '../include/header.php';

?>

<div class="container">
  <div class="row box box-mint">
   <h2>Contact Us</h2>
    <hr>
    <div class="col-md-6">
           
          <h3>24-HOUR FREECALL SUPPORT LINE: 1800 774 744</h3>
          

              <form  method="post" action="contactprocessing.php" >
                
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" name="name" class="form-control" placeholder="Name" required="required">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group" style="text-align: left;">
                      <input type="email" name="email" class="form-control" placeholder="Email" required="required">
                    </div>
                  </div>
                <div class="col-md-12">
                <div class="form-group">
                  <input type="text" name="subject" class="form-control" placeholder="Subject" required="required">
                </div>
                </div>
                 <div class="col-md-12">
                <div class="form-group">
                  <textarea name="message" id="message" class="form-control" rows="4" placeholder="Enter your message" required="required" style="border-color: #666; "></textarea>
                </div>  </div>                      
                <div class="form-group">
                 <div class="col-md-4">
                  <button type="submit" class="btn-submit pull-right">Send Now</button></form>   
                </div></div>
              
            </div>

            <div class="col-md-6">
          
               
                <ul class="address">
                  <li><i class="fa fa-map-marker"></i> <span> Address:</span> PO Box 292
                  Lutwyche QLD 4030 </li>
                  <li><i class="fa fa-phone"></i> <span> Phone:</span> +61 (07) 3857 4744 </li>
                  <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:someone@yoursite.com"> qhvsg@qhvsg.org.au</a></li>
                  <li>
                  <div class="footer-social">
                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                          
                        </div></li>
                </ul>
              </div>                            
           
  </div>

  <?php

include '../include/footer.php';
?>
