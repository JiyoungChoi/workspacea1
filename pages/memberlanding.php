<?php
include '../include/header.php';
include "../include/connect.php";
$page = "member landing";

if(!isset($_SESSION['member'])) {
    header("Location:login.php");
  }?>
  

  
  <style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: black;
  text-align: center;
  font-size: 16px;
  padding: 12px;
  width: 120px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button1 {
  display: inline-block;
  border-radius: 4px;
  background-color: red;
  border: none;
  color: black;
  text-align: center;
  font-size: 16px;
  padding: 12px;
  width: 120px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}
.button .button1 span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button .button1 span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button .button1:hover span {
  padding-right: 25px;
}

.button .button1:hover span:after {
  opacity: 1;
  right: 0;
}
</style>
<!-- PAGE HEADER -->
    <!-- PAGE HEADER -->
    <div class="page_header">
        <div class="page_header_parallax">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3><span>Services</span>My <br>Account</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="bcrumb-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="bcrumbs">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li>My account</li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<div class="container body_con" style="margin-top:20PX;">
    <div class="row">
        <div class="col-md-12">
            <div class="tab" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Account</a></li>
                    <li role="presentation"><a href="#Section2" aria-controls="edit_profile" role="tab" data-toggle="tab">Edit Details</a></li>
                    <li role="presentation"><a href="#Section3" aria-controls="profile" role="tab" data-toggle="tab">Notifications</a></li>
                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Order</a></li>
                    <li role="presentation"><a href="#Section5" aria-controls="settings" role="tab" data-toggle="tab">Schedule</a></li>
                </ul>
                
                <!-- Tab panes -->
                       <?php

$memberID = $_SESSION ['user'];
$sql = "SELECT * FROM member where memberID=$memberID";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) 
{
    $fname = $row ['firstname'];
    $lname = $row ['lastname'];
    $email = $row ['email'];
    $mobile = $row ['mobile'];
    $phone = $row ['phone'];
    $streetnum = $row ['streetnum'];
    $streetname = $row ['streetname'];
    $suburb = $row ['suburb'];
    $state = $row ['state'];
    $postcode = $row ['postcode'];
    $country = $row ['country']; 
    $poststreetnum = $row ['poststreetnum'];
    $poststreetname = $row ['poststreetname'];
    $postsuburb = $row ['postsuburb'];
    $poststate = $row ['poststate'];
    $postpostcode = $row ['postpostcode'];
    $postcountry = $row ['postcountry']; 
    $gender = $row ['gender'];
    $ispaid = $row ['membershipfee'];
    $ismember = $row ['ismember'];
    $username = $row['username'];
    $isinvited = $row['isinvited'];
    $samepostal = $row ['samepostal'];
    $ismember= $row ['ismember'];

                    ?>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" style="height:auto;" id="Section1">
                      <?php echo "<h3>Welcome $username</h3>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: lightblue;'><h4>YOUR DETAILS</h4></li>";
                            echo "<li class='list-group-item'><h4> Username: <span class='text-danger'>$username </h4></li>";
                            echo "<li class='list-group-item'><h4> Name:<span class='text-danger'> $fname $lname </h4> </li>";
                             echo "<li class='list-group-item'><h4> Gender:<span class='text-danger'> $gender </h4> </li>";
                            echo "<li class='list-group-item'><h4> Membership Status: <span class='text-danger'>";
                            if($ismember == 1){
                            echo "You are a member</h4></li></ul>";
                            } else {
                                echo "Your membership is being processed</h4></li></ul>";
                            }
                             
                            echo "<div role='tabpanel'>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: lightblue;'><h4>CONTACT DETAILS</h4></li>";
                            echo "<li class='list-group-item'><h4> E-mail: $email </h4></li>";
                            echo "<li class='list-group-item'><h4> Home Phone: $phone </h4></li>";
                            echo "<li class='list-group-item'><h4> Mobile: $mobile</h4></li></ul>";
                            echo "</div>";
                            echo "<div role='tabpanel'>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: lightblue;'><h4>ADDRESS DETAILS</h4></li>";
                            echo "<li class='list-group-item'><h4> Street Address: $streetnum $streetname </h4></li>";
                            echo "<li class='list-group-item'><h4> Suburb: $suburb </h4></li>";
                            echo "<li class='list-group-item'><h4> State: $state</h4></li>";
                            echo "<li class='list-group-item'><h4> Postcode: $postcode </h4></li>";
                            echo "<li class='list-group-item'><h4> Country: $country </h4></li></ul>";
                            echo "</div>";
                            echo "<div role='tabpanel'>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: lightblue;'><h4>POSTAL ADDRESS</h4></li>";
                            if($samepostal == 0){
                            echo "<li class='list-group-item'><h4> Street Address: $poststreetnum $poststreetname </h4></li>";
                            echo "<li class='list-group-item'><h4> Suburb: $postsuburb </h4></li>";
                            echo "<li class='list-group-item'><h4> State: $poststate</h4></li>";
                            echo "<li class='list-group-item'><h4> Postcode: $postpostcode </h4></li>";
                            echo "<li class='list-group-item'><h4> Country: $postcountry </h4></li></ul>";
                            }
                            else{
                                echo "<li class='list-group-item'><h4> AS ABOVE </h4></li></ul>";
                            }
                            echo "</div>";
                            ?>
                        </p>
                    </div>                    
                    
                    <div role="tabpanel" class="tab-pane" id="Section2">
                                <h2>UPDATING ACCOUNT</h2>
                                            <hr>
                                            <p>Update your account details.</p>

                                            <form action="accountprocessing.php" method="post">
                                                <div class="form-group">
                                                    <label>Username*</label> <input type="text"
                                                        class="form-control" name="username" 
                                                        value="<?php echo $username; ?>" readonly /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>First Name*</label> <input type="text"
                                                        class="form-control" class="form-control" name="firstname"
                                                        required value="<?php echo $row['firstname']; ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Last Name*</label> <input type="text"
                                                        class="form-control" name="lastname" required
                                                        value="<?php echo $row['lastname'] ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Street number</label> <input type="text"
                                                       class="form-control" name="streetnum"
                                                        value="<?php echo $row ['streetnum']?>" /><br /> 
                                                </div>
                                                
                                                <div class="form-group">        
                                                        <label>Street name</label><input type="text" class="form-control" name="streetname" value="<?php echo $row ['streetname']?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Suburb</label> <input type="text"
                                                        class="form-control" name="suburb"
                                                        value="<?php echo $row ['suburb']?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>State</label>
<?php
$tableName = 'member';
$colState = 'state';
function getEnumState($tableName, $colState) {
    global $con;
    $sql = "SHOW COLUMNS FROM $tableName WHERE field='$colState'";
    // retrieve enum column
    $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
    $row = mysqli_fetch_array ( $result );
    $type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
    $enumValues = substr ( $type, 0, - 1 );
    $enumExplode = explode ( ',', $enumValues );
    return $enumExplode;
}
$enumValues = getEnumState ( 'member', 'state' );
echo '<select name="state" class="form-control">';

if ((is_null ( $row ['state'] )) || (empty ( $row ['state'] ))) // if the state field is NULL or empty
{
    echo "<option value=''>Please select</option>";
} else {
    echo '<option value=" '. $row ['state'] .' "> '. $row ['state'] .' (selected)</option>'; // display the selected enum value
}

foreach ( $enumValues as $value ) {
    echo '<option value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>'; // remove the quotes from the enum values
}
echo '</select>';
?>
                                                </div>

                                                <div class="form-group">
                                                    <label>Postcode*</label> <input type="text"
                                                        class="form-control" name="postcode" required
                                                        value="<?php echo $row['postcode'] ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Country*</label> <input type="text" name="country"
                                                        class="form-control" required
                                                        value="<?php echo $row['country'] ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Phone</label> <input type="text" name="phone"
                                                        class="form-control"
                                                        value="<?php
                                                        
                                                        echo $row ['phone']?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Mobile</label> <input type="text" name="mobile"
                                                        class="form-control"
                                                        value="<?php
                                                        echo $row ['mobile']?>" /><br />
                                                </div>


                                                <div class="form-group">
                                                    <label>Email*</label><br><input type="email" name="email"
                                                        class="form-control" required
                                                        value="<?php echo $row['email'] ?>" /><br />
                                                </div>

                                                <div class="form-group">
                                                    <label>Gender*</label>
<?php
// generate drop-down list for gender using enum data type and values from database
$tableName = 'member';
$colGender = 'gender';
function getEnumGender($tableName, $colGender) {
    global $con; // enable database connection in the function
    $sql = "SHOW COLUMNS FROM $tableName WHERE field='$colGender'";
    // retrieve enum column
    $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
    // run the query
    $row = mysqli_fetch_array ( $result ); // store the results in a variable named $row
    $type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
    $enumValues = substr ( $type, 0, - 1 ); // return the enum string
    $enumExplode = explode ( ',', $enumValues ); // split the enum string into individual values
    return $enumExplode; // return all the enum individual values
}
$enumValues = getEnumGender ( 'member', 'gender' );
echo '<select name="gender" class="form-control">';

echo '<option value=" '. $row ['gender'] .' "> '. $row ['gender'] .' (selected) </option>'; // display the selected enum value

foreach ( $enumValues as $value ) {
    echo '<option value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>';
}
echo '</select>';
?></div>

                                                <input type="hidden" name="memberID"
                                                    value="<?php echo $memberID; ?>"> <input type="submit"
                                                    name="accountupdate" value="Update Account" class="btn-submit" />
                                            </form>

                                      
</br>
        <h3>Update Password</h3>
                                        <p>Passwords must have a minimum of 8 characters.</p>
                                        <form action="accountpasswordprocessing.php" method="post">
                                            <label>New Password*</label> <input type="password"
                                                name="password" pattern=".{8,}"
                                                title="Password must be 8 characters or more" required /><br />
                                            <input type="hidden" name="memberID"
                                                value="<?php echo $memberID; ?>">
                                            <p>
                                            
                                            
                                            <div class="form-group">
                                                <input type="submit" name="passwordupdate"
                                                    value="Update Password" class="btn-submit" />
                                            </div>
                                            
                                        </form>

</p></form></div>

                    <div role="tabpanel" class="tab-pane" id="Section3">
                       
                        <p>
                           <?php 
    
	   //                     $sql = "SELECT * FROM member ";
				// 			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
				// 			$row_count = mysqli_num_rows ( $result );
							
				// 			while ( $row = mysqli_fetch_array ( $result ) ) {
				// 			    if ($row ['isinvited'] == 1) {
							     
				// 			    $decline='	
				// 			<form method="post" action="run.php">    
				// 			    <input style="position: absolute; visibility: hidden;" type="text" name="username" value="'.$username.'"/>
			 //                   <input  style="text-decoration:none; color:white;" type="submit" name="invbtn" class="button1" value="Decline"/>
		  //                  </form>';
				// 			}

				// 			}
	echo "<h4>Notifications</h4>";
	echo "<ul class='list-group'>";

		echo "
			<li class='list-group-item'><h5>*membership fee need to be paid</li>";

	if ($ismember == 0) {
		echo "<li class='list-group-item'><span class='text-danger'><h5>*Your application is still processing</li>";
	}
	if ($ismember == 1) {
		echo "<li class='list-group-item'><span class='text-danger'><h5>*congratulations! You are now a member of QHVSG.</li>";
	}
    if ($isinvited == '1') {
	echo "<li class='list-group-item'>
		<a href='chat/practice.php' style='text-decoration:none; color:black;'><span class='text-danger'><h5>You have been invited into a Private chatroom <br><button class='button' style='vertical-align:middle; margin-left:5px;'><span> Accept</span></button></a>
			<form method='post' action='run.php'>    
			<input style='position: absolute; visibility: hidden;' type='text' name='username' value='".$username."'/>
			 <input  style='text-decoration:none; color:white;' type='submit' name='invbtn' class='button1' value='Decline'/>
		  </form>
		</li>";
		
    }



?>
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Section4">
                        <h3>ORDER</h3>
                        <p>
                            3 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean varius quis ipsum a rutrum. Donec quis consequat sem. Donec gravida nisi quis nibh vestibulum ullamcorper. Suspendisse turpis sapien, lobortis vel.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Section5">
                        <h3>SCHEDULE</h3>
                        <p>
                            4 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas bibendum ante in velit faucibus, a efficitur nisl vestibulum. Sed a nisi eget tellus eleifend mattis. Nam vulputate leo in posuere imperdiet. Mauris a mollis nulla. Nulla euismod sed neque eget lacinia. Donec at lacus et arcu consectetur aliquet ac sed tellus. Donec non dapibus turpis
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
</br>
	  
			

		<?php } ?>
				
				
	




<?php require '../include/footer.php';?>