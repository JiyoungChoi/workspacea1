

<?php

include '../../include/connect.php';
include 'admin_header.php'; // includes a session_start()


?>

<div class="content">
          <div class="container-fluid">
              <div class="row">

<div class="col-lg-8 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Add New Events</h4>
                        </div>
                        <div class="content">

				<form action="eventadd_processing.php" method="post"
					enctype='multipart/form-data'>

          <div class="row">
              <div class="col-md-12">
					<div class="form-group">
						<label>Title*</label> <input class="form-control" type="text"
							name="title" required><br />
					</div></div></div>

          <div class="row">
              <div class="col-md-12">
					<div class="form-group">
						<label>Content*</label>
						<textarea rows="10" cols="60%" class="form-control" name="content"
							required></textarea>

					</div></div></div>

          <div class="row">
              <div class="col-md-12">
					<div class="form-group">
						<label>venue</label> <input type="text" name="venue"
							class="form-control" /><br />
					</div></div></div>

          <div class="row">
              <div class="col-md-12">
					<div class="form-group">
						<label>date</label> <input type="text" name="eventdate"
							placeholder="yyyy-mm-dd 00:00:00" class="form-control" /><br />
					</div></div></div>

          <div class="row">
              <div class="col-md-12">
					<div class="form-group">
						<label>Admin</label>
						<!-- create a drop-down list populated by the admin details stored in the database -->
						<select name='adminID' class="form-control" required>
							<option value="">Please select</option>

 <?php
	$sql = "SELECT * FROM admin where type=3";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	while ( $row = mysqli_fetch_array ( $result ) ) {
		echo "<option value=" . $row ['adminID'] . ">" . $row ['firstname'] . " " . $row ['lastname'] . "</option>";
	}
	?>

 </select><br />
					</div></div></div>

          <div class="row">
              <div class="col-md-12">
					<div class="form-group">

						<label>New Image</label> <input class="form-control" type="file"
							name="image"><br />
						<p>Accepted files are JPG, GIF or PNG. Maximum size is 500kb.</p>
					</div></div></div>



					<input type="hidden" name="eventID" value="<?php echo $eventID; ?>">

<div class="text-center">
					<input type="submit" class="form-control" name="Adding new event"
						value="Adding new event" />

			</div>
			</form>


		</div>
	</div>
</div>
