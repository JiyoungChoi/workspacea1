
<?php
$page = "update member";

include "../../include/connect.php";
include 'admin_header.php';


?>
<?php
$volunteerID = $_GET ['volunteerID'];
$sql = "SELECT * FROM volunteer WHERE volunteerID = '$volunteerID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$row = mysqli_fetch_array ( $result );
$applifile = $row ['application'];
$username = $row ['username'];
$fname = $row ['firstname'];
$lname = $row ['lastname'];
$email = $row ['email'];
$phone = $row ['phone'];
$streetnum = $row ['address_line1'];
$streetname = $row ['address_line2'];
$suburb = $row ['city'];
$state = $row ['state'];
$postcode = $row ['postcode'];
$gender = $row ['gender'];
$isvolunteer = $row ['isvolunteer'];
$isinvited = $row['isinvited'];


?>
<div class="content-wrapper" style="padding: 1% 5% 5% 5%;">
	<h1>Update Volunteer</h1>
	<form action="newmemberupdateprocessing.php?volunteerID={$row['volunteerID']}" method="post">
	                     <?php 
                            echo "<ul class='list-group' style>";
                            echo "<li class='list-group-item' Style='background-color: black; color: white;'> MEMBERS DETAILS </li>";
                            echo "<li class='list-group-item'> Username: <input class='form-control' type='text' name='username' required value='$username' readonly /> </li>";
                            echo "<li class='list-group-item'>  First Name:<input type='text' class='form-control' class='form-control' name='firstname' required value='$fname'</li>";
                            echo "<li class='list-group-item'>  Last Name:<input type='text'class='form-control'  name='lastname' required value='$lname' /> </li>";
                            echo "<li class='list-group-item'>  Gender:";  
                            echo '<br><input type="radio" id="gender" name="gender" value="male"';
                            if($gender == 'Male'){
                                echo "checked='checked'";
                                echo '> Male (Selected)<br>';
                            } else{
                                echo '> Male<br>';
                            }
                           
                            echo '<input type="radio" id="gender" name="gender" value="female"';
                            if($gender == 'Female'){
                                echo "checked='checked'";
                                 echo '> Female (Selected)<br> </li>';
                            } else{
                            echo '> Female<br> </li>';
                            }
                            echo "<li class='list-group-item'>  Volunteer Approval Status:";
                            echo '<br><input type="radio" id="isvolunteer" name="isvolunteer" value="0"';
                            if($isvolunteer == 0){
                                echo "checked='checked'";
                                
                
                            echo '> Not Approved (Selected) <br>';
                            } else{
                                  echo '> Not Approved <br>';
                            }
                            echo '<input type="radio" id="isvolunteer" name="isvolunteer" value="1"';
                            if($isvolunteer == 1){
                                echo "checked='checked'";
                                echo '> Approved (Selected) <br> </li><br>'; 
                            }else{
                                 echo '> Approved <br> </li><br>'; 
                            }
                            

                             
                            echo "<div role='tabpanel'>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: black; color: white;'> CONTACT DETAILS </li>";
                            echo "<li class='list-group-item'>  E-mail: <input type='email' name='email' class='form-control' required value='$email' readonly/></li>";
                            echo "<li class='list-group-item'>  Contact Number: <input type='text' name='phone' class='form-control' value='$phone' /></li>";
                            echo "</div>";
                            echo "<div role='tabpanel'>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: Black; color: white;'> ADDRESS DETAILS </li>";
                            echo "<li class='list-group-item'>  Adddress Line 1:  <input type='text' class='form-control' name='streetnum' value='$streetnum' /> </li>";
                            echo "<li class='list-group-item'>  Addresss Line 2:  <input type='text' class='form-control' name='streetname' value='$streetname' /> </li>";
                            echo "<li class='list-group-item'>  City/Suburb:<input type='text' class='form-control' name='suburb' value='$suburb'/></li>";
                            echo "<li class='list-group-item'>  State: &nbsp; &nbsp;";
                            echo "<select name='state'>";
                            echo "<option disabled selected='selected'> $state (Selected)</option>";
                            echo "<option id='state' name='state' value='QLD'>QLD</option>";
                            echo "<option id='state' name='state' value='NSW'>NSW</option>";
                            echo "<option id='state' name='state' value='VIC'>VIC</option>";
                            echo "<option id='state' name='state' value='SA'>SA</option>";
                            echo "<option id='state' name='state' value='WA'>WA</option>";
                            echo "<option id='state' name='state' value='TAS'>TAS</option>";
                            echo "<option id='state' name='state' value='NT'>NT</option>";
                            echo "</select>";
                            echo "</li>";
                            echo "<li class='list-group-item'>  Postcode: <input type='text'class='form-control' name='postcode' required value='$postcode' /> </li>";
                            echo "</div>";
                            ?>
                            <input type="submit" class="btn btn-primary" name="updateaccount" class="form-control" value="Update Account" />
	</form>

</div>


