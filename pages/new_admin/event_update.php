
<?php


include 'admin_header.php'; // includes a session_start()
include '../../include/connect.php';
?>


<div class="content">
          <div class="container-fluid">
              <div class="row">

<div class="col-lg-8 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Events</h4>
                        </div>
                        <div class="content">


<?php
$eventID = $_GET ['eventID'];
$sql = "SELECT events.* ,admin.firstname FROM events JOIN admin USING (adminID)  WHERE eventID = '$eventID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$row = mysqli_fetch_array ( $result );
?>




			<form action="eventupdate_processing.php" method="post">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">

          					<label>Title*</label> <input class="form-control" type="text"
          						name="title" required value="<?php echo $row ['title']?>" /><br />
          				</div></div></div>

                  <div class="row">
                      <div class="col-md-12">
				<div class="form-group">
					<label>Content*</label>
					<textarea rows="10" cols="60%" class="form-control" name="content"
						required> <?php echo $row ['content']?></textarea>

				</div></div></div>

        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label>venue</label> <input type="text" name="venue"
						class="form-control" required
						value="<?php
						echo $row ['venue']?>" /><br />
				</div></div></div>



        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label>date</label> <input type="text" name="eventdate"
						class="form-control" required
						value="<?php
						echo $row ['eventdate']?>" /><br />
				</div></div></div>

        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label>Admin*</label> <select name='adminID' class="form-control" required>
						<option value="<?php echo $row['adminID'] ?>"><?php echo $row ['firstname']?></option>

            <?php
    						$sql = "SELECT * FROM admin where type=3";
    						$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
    						while ( $row = mysqli_fetch_array ( $result ) ) {
    							echo "<option value=" . $row ['adminID'] . ">" . $row ['firstname'] . " " . "</option>";
    						}
						?>
            </select><br />
				</div></div></div>
        <input type="hidden" name="eventID" value="<?php echo $eventID; ?>">

        <div class="text-center">
            <button type="submit" class="btn btn-info btn-fill btn-wd" name="submit">Update event</button>
        </div>
      </form>


</div></div></div></div></div></div>

<div class="col-lg-8 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Update Image</h4>
                        </div>
                        <div class="content">

      <div class="row">
          <div class="col-md-12">
            <label>Update Image</label>
              <div class="form-group">

          <?php
										$sql = "SELECT events.* FROM events WHERE eventID ='$eventID'";
										$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
										$row = mysqli_fetch_array ( $result );

										if ((is_null ( $row ['eventimg'] )) || (empty ( $row ['eventimg'] ))) // if the photo field is NULL or empty
{
											echo "<p><img src='../img/default.png' width=620 height=300 alt='default photo'/></p>"; // display the default photo
										} else {
											echo "<img src='../../images/blog/" . ($row ['eventimg']) . "'" . "style=' height:300px;' background-size='cover'>";
										}
										?>
</div></div>
</div>




<?php
if ((is_null ( $row ['eventimg'] )) || (empty ( $row ['eventimg'] ))) // if the photo field is NULL or empty
{
echo "<p><img src='../images/blog.png' width=150 height=150
alt='default photo'  /></p>";
} else

{
echo "<p><img src='../images/blog/" . ($row ['eventimg']) . "'" . '
width=150 height=150 alt="contact photo"' . "/></p>";
}
?>

<form action="eventupdateimg_processing.php" method="post"
                  enctype="multipart/form-data">

                  <input type="hidden" name="eventID"
                    value="<?php echo $eventID; ?>">

                  <div class="form-group">
                    <label>New Image</label> <input type="file" name="image"
                      class="form-control" /><br />
                    <p>Accepted files are JPG, GIF or PNG. Maximum size is
                      500kb.</p>
                  </div>

                  <div class="text-center">
                  <button type="submit" class="btn btn-info btn-fill btn-wd" name="imageupdate">Update event</button>


                </form>


              </div>
            </div>
