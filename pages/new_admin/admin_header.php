<?php
session_start();
if (!(isset($_SESSION['admin'])||isset($_SESSION['board'])))  
 {
header("Location:admin_login.php");
}
  include "../../include/connect.php";
$page = basename($_SERVER['PHP_SELF'], ".php");
?>


<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>QHVSG ADMIN</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="black" data-active-color=" info">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="admin_index.php" class="simple-text">
              QHVSG
                </a>
            </div>

            <ul class="nav">

              <li class="<?= ($page == 'admin_index') ? 'active':''; ?>">
                  <a href="admin_index.php">
                      <i class="ti-pie-chart"></i>
                      <p>Dashboard</p>

                  </a>

              </li>
                    <li class="<?= ($page == 'member_manage') ? 'active':''; ?>">
                  <a href="member_manage.php">
                      <i class="ti-user"></i>
                      <p>Members</p>
                  </a>
              </li>
              
                <li class="<?= ($page == 'volunteer_manage') ? 'active':''; ?>">
                  <a href="volunteer_manage.php">
                      <i class="ti-hand-open"></i>
                      <p>Volunteers</p>
                  </a>
              </li>
              
              <li class="<?= ($page == 'event_manage') ? 'active':''; ?>">
                      <a href="event_manage.php">
                      <i class="ti-calendar"></i>
                      <p>Events</p>
                  </a>

              </li>
              <li class="<?= ($page == 'donation_manage') ? 'active':''; ?>">
                      <a href="donation_manage.php">
                      <i class="ti-money"></i>
                      <p>donation</p>
                  </a>

              </li>


            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header ">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only"><?php echo $page?></span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <?php
                                   if (isset ( $_SESSION ['admin'] )) {
                                      $username = $_SESSION ['admin'];
                                      $sql = "SELECT * FROM admin  WHERE username= '$username'";
                                      $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
                                      $row = mysqli_fetch_array ( $result );
                                      // check to see if a member or admin is logged in and, if so, display the logged in menu items

                                      echo '  <a class="navbar-brand text-uppercase" href="admin_account.php">' . $row ['firstname'] . '</a>';}?>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="../index.php">
                                <i class="ti-world"></i>
								<p>QHVSG Homepage</p>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
								<p>Stats</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">5</p>
									<p>Notifications</p>
									<b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
						<li>
                            <a href="admin_account.php">
								<i class="ti-settings"></i>
								<p>Account</p>
                            </a>
                        </li>
                        <li>
                            <a href="../logout.php">
                                <i class="ti-lock"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
