
<?php

include '../../include/connect.php';
include 'admin_header.php'; // includes a session_start()


?>
  <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                              <?php
              // user messages
              if (isset ( $_SESSION ['msg'] )) // if session error is set
              {
                echo '<div class="alert alert-success">
                                  <button type="button" aria-hidden="true" class="close">×</button>
                                  <span><b> Success - ' .$_SESSION ['msg'].'</b></span>
                              </div>';
                unset ( $_SESSION ['msg'] ); // unset session error
              }

              ?>
                                <h4 class="title">Events</h4>


                            </div>

                            <div class="content table-responsive table-full-width">
                                  <a href="event_add.php" class="btn btn-outline pull-right">Add New Event</a>
                                <table class="table table-striped">
		<thead>
			<tr>
				<th>EventID</th>
				<th>Title</th>
				<th>Description</th>
				<th>Date</th>
				<th>Venue</th>
				<th>Staff</th>
				<th>Contact info</th>
				<th>modifying</th>

			</tr>
		</thead>



<?php
$sql = "SELECT events.*, admin.firstname, admin.email FROM events JOIN admin USING (adminID)  WHERE events.adminID = admin.adminID";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$numrow = mysqli_num_rows ( $result ); // retrieve the number of rows
echo "<samll>There are currently <strong>" . $numrow . "</strong>
	events.</small>";

while ( $row = mysqli_fetch_array ( $result ) ) {
	echo "<tr>";
	echo "<td>" . $row ['eventID'] . "</td>";
	echo "<td>" . $row ['title'] . "</td>";
	echo "<td>" . (substr ( ($row ['content']), 0, 50 )) . "...</td>";
	echo "<td>" . date ( "d/m/y H", strtotime ( $row ['eventdate'] ) ) . "</td>";
	echo "<td>" . $row ['venue'] . "</td>";
	echo "<td>" . $row ['firstname'] . "</td>";
	echo "<td>" . $row ['email'] . "</td>";


  echo "<td class='text-danger '><a href=\"event_update.php?eventID={$row['eventID']}\"><u>Update</u></a>
  <a href=\"event_delete.php?eventID={$row['eventID']}\" onclick=\"return confirm('Are you sure you want to delete this event?')\"><u>Delete</u></a></td>";
	echo "</tr>";
}

?>
 			</table>



<?php
// user messages
if (isset ( $_SESSION ['msg'] ))
{
  echo '<div class="alert alert-success">
                    <button type="button" aria-hidden="true" class="close">×</button>
                    <span><b> Success - </b> This is a regular notification made with ".alert-success"</span>
                </div>';
  unset ( $_SESSION ['msg'] );
}

?>


</div></div></div>

</div></div></div>
