
<?php
$page = "update member";

include "../../include/connect.php";
include 'admin_header.php';


?>
<?php
$memberID = $_GET ['memberID'];
$sql = "SELECT * FROM member WHERE memberID = '$memberID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$row = mysqli_fetch_array ( $result );
$applifile = $row ['application'];
$username = $row ['username'];
$fname = $row ['firstname'];
$lname = $row ['lastname'];
$email = $row ['email'];
$mobile = $row ['mobile'];
$phone = $row ['phone'];
$streetnum = $row ['streetnum'];
$streetname = $row ['streetname'];
$suburb = $row ['suburb'];
$state = $row ['state'];
$postcode = $row ['postcode'];
$country = $row ['country']; 
$poststreetnum = $row ['poststreetnum'];
$poststreetname = $row ['poststreetname'];
$postsuburb = $row ['postsuburb'];
$poststate = $row ['poststate'];
$postpostcode = $row ['postpostcode'];
$postcountry = $row ['postcountry']; 
$gender = $row ['gender'];
$ispaid = $row ['membershipfee'];
$ismember = $row ['ismember'];
$username = $row['username'];
$isinvited = $row['isinvited'];
$samepostal = $row ['samepostal'];
$ismember= $row ['ismember'];

?>
<div class="content-wrapper" style="padding: 1% 5% 5% 5%;">
	<h1>Update member</h1>
	<form action="newmemberupdateprocessing.php?memberID={$row['memberID']}" method="post">
	                     <?php 
                            echo "<ul class='list-group' style>";
                            echo "<li class='list-group-item' Style='background-color: black; color: white;'> MEMBERS DETAILS </li>";
                            echo "<li class='list-group-item'> Username: <input class='form-control' type='text' name='username' required value='$username' readonly /> </li>";
                            echo "<li class='list-group-item'>  First Name:<input type='text' class='form-control' class='form-control' name='firstname' required value='$fname'</li>";
                            echo "<li class='list-group-item'>  Last Name:<input type='text'class='form-control'  name='lastname' required value='$lname' /> </li>";
                            echo "<li class='list-group-item'>  Gender:";  
                            echo '<br><input type="radio" id="gender" name="gender" value="male"';
                            if($gender == 'Male'){
                                echo "checked='checked'";
                                echo '> Male (Selected)<br>';
                            } else{
                                echo '> Male<br>';
                            }
                           
                            echo '<input type="radio" id="gender" name="gender" value="female"';
                            if($gender == 'Female'){
                                echo "checked='checked'";
                                 echo '> Female (Selected)<br> </li>';
                            } else{
                            echo '> Female<br> </li>';
                            }
                            echo "<li class='list-group-item'>  Membership Status:";
                            echo '<br><input type="radio" id="ismember" name="ismember" value="0"';
                            if($ismember == 0){
                                echo "checked='checked'";
                                
                
                            echo '> Not Approved (Selected) <br>';
                            } else{
                                  echo '> Not Approved <br>';
                            }
                            echo '<input type="radio" id="ismember" name="ismember" value="1"';
                            if($ismember == 1){
                                echo "checked='checked'";
                                echo '> Approved (Selected) <br> </li><br>'; 
                            }else{
                                 echo '> Approved <br> </li><br>'; 
                            }
                            

                             
                            echo "<div role='tabpanel'>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: black; color: white;'> CONTACT DETAILS </li>";
                            echo "<li class='list-group-item'>  E-mail: <input type='email' name='email' class='form-control' required value='$email' readonly/></li>";
                            echo "<li class='list-group-item'>  Home Phone: <input type='text' name='phone' class='form-control' value='$phone' /></li>";
                            echo "<li class='list-group-item'>  Mobile:<input type='text' name='mobile' class='form-control' value='$mobile' /></li></ul>";
                            echo "</div>";
                            echo "<div role='tabpanel'>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: Black; color: white;'> ADDRESS DETAILS </li>";
                            echo "<li class='list-group-item'>  Street Number:  <input type='text' class='form-control' name='streetnum' value='$streetnum' /> </li>";
                            echo "<li class='list-group-item'>  Street Name:  <input type='text' class='form-control' name='streetname' value='$streetname' /> </li>";
                            echo "<li class='list-group-item'>  Suburb:<input type='text' class='form-control' name='suburb' value='$suburb'/></li>";
                            echo "<li class='list-group-item'>  State: &nbsp; &nbsp;";
                            echo "<select name='state'>";
                            echo "<option disabled selected='selected'> $state (Selected)</option>";
                            echo "<option id='state' name='state' value='QLD'>QLD</option>";
                            echo "<option id='state' name='state' value='NSW'>NSW</option>";
                            echo "<option id='state' name='state' value='VIC'>VIC</option>";
                            echo "<option id='state' name='state' value='SA'>SA</option>";
                            echo "<option id='state' name='state' value='WA'>WA</option>";
                            echo "<option id='state' name='state' value='TAS'>TAS</option>";
                            echo "<option id='state' name='state' value='NT'>NT</option>";
                            echo "</select>";
                            echo "</li>";
                            echo "<li class='list-group-item'>  Postcode: <input type='text'class='form-control' name='postcode' required value='$postcode' /> </li>";
                            echo "<li class='list-group-item'>  Country: <input type='text' name='country' class='form-control' required value='$country' /> </li></ul>";
                            echo "</div>";
                            echo "<div role='tabpanel'>";
                            echo "<ul class='list-group'>";
                            echo "<li class='list-group-item' Style='background-color: black; color: white;'> POSTAL ADDRESS </li>";
                             echo "<li class='list-group-item'>  Street Number:  <input type='text' class='form-control' name='poststreetnum' value='$poststreetnum' /> </li>";
                            echo "<li class='list-group-item'>  Street Name:  <input type='text' class='form-control' name='poststreetname' value='$poststreetname' /> </li>";
                            echo "<li class='list-group-item'>  Suburb:<input type='text' class='form-control' name='postsuburb' value='$postsuburb' /></li>";
                            echo "<li class='list-group-item'>  State: &nbsp; &nbsp;";
                            echo "<select name='poststate'>";
                            echo "<option disabled selected='selected'> $poststate (Selected)</option>";
                            echo "<option id='poststate' name='poststate' value='QLD'>QLD</option>";
                            echo "<option id='poststate' name='poststate' value='NSW'>NSW</option>";
                            echo "<option id='poststate' name='poststate' value='VIC'>VIC</option>";
                            echo "<option id='poststate' name='poststate' value='SA'>SA</option>";
                            echo "<option id='poststate' name='poststate' value='WA'>WA</option>";
                            echo "<option id='poststate' name='poststate' value='TAS'>TAS</option>";
                            echo "<option id='poststate' name='poststate' value='NT'>NT</option>";
                            echo "</select>";
                            echo "</li>";
                            echo "<li class='list-group-item'>  Postcode: <input type='text' class='form-control' name='postcode'  value='$postpostcode' /> </li>";
                            echo "<li class='list-group-item'>  Country: <input type='text' name='postcountry' class='form-control'  value='$postcountry' /> </li></ul>";
                            echo "</div>";
                            ?>
                            <input type="submit" class="btn btn-primary" name="updateaccount" class="form-control" value="Update Account" />
	</form>

</div>


