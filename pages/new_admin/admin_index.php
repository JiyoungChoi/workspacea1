<?php
include 'admin_header.php';
$page ="admin_index";
include "../../include/connect.php";
 ?>

 <div class="content">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-lg-3 col-sm-6">
                       <div class="card">
                           <div class="content">
                               <div class="row">
                                   <div class="col-xs-5">
                                       <div class="icon-big icon-warning text-center">
                                           <i class="ti-user"></i>
                                       </div>
                                   </div>
                                   <div class="col-xs-7">
                                       <div class="numbers">
                                           <p>Members</p>
                                           <?php
                                             $sql = "SELECT * FROM member ";
                                             $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
                                             $row_count = mysqli_num_rows ( $result );
                                             echo   $row_count ?>
                                       </div>
                                   </div>
                               </div>
                               <div class="footer">
                                   <hr />
                                   <div class="stats">
                                       <a href="../new_admin/member_manage.php" ><i class="ti-reload"></i>See detail</a>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>

                     <div class="col-lg-3 col-sm-6">
                                           <div class="card">
                                               <div class="content">
                                                   <div class="row">
                                                       <div class="col-xs-5">
                                                           <div class="icon-big icon-success text-center">
                                                               <i class="ti-hand-open"></i>
                                                           </div>
                                                       </div>
                                                       <div class="col-xs-7">
                                                           <div class="numbers">
                                                              <p>Volunteers</p>
                                                              <?php
                                                                $sql = "SELECT * FROM volunteer ";
                                                                $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
                                                                $row_count = mysqli_num_rows ( $result );
                                                                echo   $row_count ?>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="footer">
                                                       <hr />
                                                       <div class="stats">
                                                           <a href="../new_admin/volunteer_manage.php" ><i class="ti-reload"></i> See detail</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>


                   <div class="col-lg-3 col-sm-6">
                       <div class="card">
                           <div class="content">
                               <div class="row">
                                   <div class="col-xs-5">
                                       <div class="icon-big icon-success text-center">
                                           <i class="ti-wallet"></i>
                                       </div>
                                   </div>
                                   <div class="col-xs-7">
                                       <div class="numbers">
                                          <p>Donation</p>
                                         <?php
                                          $sql = "SELECT SUM(PaymentAmount)  as sum FROM donation_nonmember ";
                                          $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
                                        $row = mysqli_fetch_array ( $result );
                                        echo ' '.$row ['sum'];?>




                                       </div>
                                   </div>
                               </div>
                               <div class="footer">
                                   <hr />
                                   <div class="stats">
                                       <i class="ti-reload"></i> See detail
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>


                   <div class="col-lg-3 col-sm-6">
                       <div class="card">
                           <div class="content">
                               <div class="row">
                                   <div class="col-xs-5">
                                       <div class="icon-big icon-success text-center">
                                           <i class="ti-calendar"></i>
                                       </div>
                                   </div>
                                   <div class="col-xs-7">
                                       <div class="numbers">
                                          <p>Events</p>
                                          <?php
                                            $sql = "SELECT * FROM events ";
                                            $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
                                            $row_count = mysqli_num_rows ( $result );
                                            echo   $row_count ?>
                                       </div>
                                   </div>
                               </div>
                               <div class="footer">
                                   <hr />
                                   <div class="stats">
                                       <a href="../new_admin/event_manage.php" ><i class="ti-reload"></i> See detail</a>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>







                   </div>
               </div>

</div></div></div>

  <?php
  include 'admin_footer.php'; ?>
