
<?php
session_start ();
include "../../include/connect.php";

?>
<?php

$username = mysqli_real_escape_string ( $con, $_POST ['username']);
$contactnumber = mysqli_real_escape_string ( $con, $_POST ['contactnumber']); // prevent SQL injection
$fname = mysqli_real_escape_string ( $con, $_POST ['firstname']);
$lname = mysqli_real_escape_string ( $con, $_POST ['lastname']);
$email = mysqli_real_escape_string ( $con, $_POST ['email']);
$mobile = mysqli_real_escape_string ( $con, $_POST ['mobile']);
$phone = mysqli_real_escape_string ( $con, $_POST ['phone']);
$streetnum = mysqli_real_escape_string ( $con, $_POST ['streetnum']);
$streetname = mysqli_real_escape_string ( $con, $_POST ['streetname']);
$suburb = mysqli_real_escape_string ( $con, $_POST ['suburb']);
$state = mysqli_real_escape_string ( $con, $_POST ['state']);
$postcode = mysqli_real_escape_string ( $con, $_POST ['postcode']);
$country = mysqli_real_escape_string ( $con, $_POST ['country']); 
$poststreetnum = mysqli_real_escape_string ( $con, $_POST ['poststreetnum']);
$poststreetname = mysqli_real_escape_string ( $con, $_POST ['poststreetname']);
$postsuburb = mysqli_real_escape_string ( $con, $_POST ['postsuburb']);
$poststate = mysqli_real_escape_string ( $con, $_POST ['poststate']);
$postpostcode = mysqli_real_escape_string ( $con, $_POST ['postcode']);
$postcountry = mysqli_real_escape_string ( $con, $_POST ['postcountry']); 
$gender = mysqli_real_escape_string ( $con, $_POST ['gender']);
$ismember = mysqli_real_escape_string ( $con, $_POST ['ismember']);
$isvolunteer = mysqli_real_escape_string ( $con, $_POST ['isvolunteer']);
$volunteerID = $_GET ['volunteerID'];
$memberID = $_GET ['memberID'];

if(isset($memberID)){
	$sql = "UPDATE  member  SET firstname = '$fname', lastname ='$lname', streetnum ='$streetnum', streetname ='$streetname', suburb ='$suburb', state ='$state', postcode ='$postcode', country ='$country', poststreetnum ='$poststreetnum', poststreetname ='$poststreetname', postsuburb ='$postsuburb', poststate ='$poststate', postpostcode ='$postpostcode', postcountry ='$postcountry', phone ='$phone', mobile ='$mobile', gender ='$gender', ismember ='$ismember' WHERE username ='$username'";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	if ($result == 1) {
		$_SESSION ['msg'] = 'Member ID ' . $memberID . ' updated successfully';  
		header ( "location:member_manage.php" ); 
	} else {
		$_SESSION ['msg'] = 'member ID ' . $memberID . ' updated failed';
	}
}

if(isset($volunteerID)){
	$sql = "UPDATE  volunteer  SET firstname = '$fname', lastname ='$lname', address_line1 ='$streetnum', address_line2 ='$streetname', city ='$suburb', state ='$state', postcode ='$postcode', phone ='$phone', gender ='$gender', isvolunteer ='$isvolunteer' WHERE username ='$username'";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	if ($result == 1) {
		$_SESSION ['msg'] = 'volunteer ID ' . $volunteerID . ' updated successfully';  
		header ( "location:volunteer_manage.php" ); 
	} else {
		$_SESSION ['msg'] = 'volunteer ID ' . $volunteerID . ' updated failed';
	}
}
?>
