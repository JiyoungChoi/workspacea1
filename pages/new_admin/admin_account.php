<?php

include 'admin_header.php';
include "../../include/connect.php";



?>
<?php

                            $username = $_SESSION ['admin'];
                            $sql = "SELECT * FROM admin  WHERE username= '$username'";
                            $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
                            $row = mysqli_fetch_array ( $result );?>

<div class="content">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-4 col-md-5">
                      <div class="card card-user">
                          <div class="image">
                              <img src="../../images/logo.png" alt="..."/>
                          </div>
                          <div class="content">
                              <div class="author">
                                <img class="avatar border-white" src="assets/img/faces/face-0.jpg" alt="..."/>
                                <h4 class="title"> <?php echo $row ['firstname'] ?><br />

                                </h4>
                              </div>
                              <p class="description text-center">
                              STAFF
                              </p>
                          </div>
                          <hr>

                      </div>
                      <div class="card">
                          <div class="header">
                              <h4 class="title">Team Members</h4>
                          </div>
                          <div class="content">
                              <ul class="list-unstyled team-members">
                          <?php

                                                      $username = $_SESSION ['admin'];
                                                      $sql = "SELECT * FROM admin";
                                                      $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
                                                      while ( $row = mysqli_fetch_array ( $result ) )

                                                  		{?>

                                          <li>
                                              <div class="row">
                                                  <div class="col-xs-3">
                                                      <div class="avatar">
                                                          <img src="assets/img/faces/face-0.jpg" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-6">
                                                   <?php echo $row ['firstname'] ?>

                                                  </div>

                                                  <div class="col-xs-3 text-right">
                                                      <btn class="btn btn-sm btn-success btn-icon"><a href="mailto:<?php  echo $row ['email'] ?>" target="_top"><i class="fa fa-envelope"></i></a></btn>
                                                  </div>
                                              </div>
                                          </li>
                                        <?php } ?>
                                      </ul>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-8 col-md-7">
                      <div class="card">
                          <div class="header">
                              <h4 class="title">Edit Profile</h4>
                          </div>
                          <div class="content">
                            <?php

                                      $username = $_SESSION ['admin'];
                                                        $sql = "SELECT * FROM admin  WHERE username= '$username'";
                                                        $result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query

                                                      $row = mysqli_fetch_array ( $result );
                                                      $fname = $row ['firstname'];
                            $lname = $row ['lastname'];
                            $email = $row ['email'];
                            $phone = $row ['phone'];
                            $username = $row ['username'];
                             $adminID = $row ['adminID'];?>
                              <form action="admin_accountprocessing.php"  method="post">
                                  <div class="row">

                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label>Username</label>
                                              <input type="text" class="form-control border-input" name="username" value="<?php echo $username; ?>" readonly>
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label for="exampleInputEmail1">Email address</label>
                                              <input type="email" class="form-control border-input" name="email" value="<?php echo $email; ?>">
                                          </div>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label>First Name</label>
                                              <input type="text" class="form-control border-input" name="firstname"  value="<?php echo $fname; ?>">
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label>Last Name</label>
                                              <input type="text" class="form-control border-input" name="lastname" value="<?php echo $lname; ?>">
                                          </div>
                                      </div>
                                  </div>



                                  <div class="row">
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              <label>Phone</label>
                                              <input type="text" class="form-control border-input" name="phone"  value="<?php
                                                echo $phone ?>" />
                                          </div>
                                      </div>


                                  </div>


                                  <div class="text-center">
                                      <button type="submit" class="btn btn-info btn-fill btn-wd">Update Profile</button>
                                  </div>
                                  <input type="hidden" name="adminID"
                                      value="<?php echo   $adminID; ?>">


                                  <div class="clearfix"></div>
                              </form>
                          </div>
                      </div>
                  </div>


                  <div class="col-lg-8 col-md-7">
                      <div class="card">
                          <div class="header">
                              <h4 class="title">Change Password</h4>
                          </div>
                          <div class="content">
                            <p>Passwords must have a minimum of 8 characters.</p>
                            <form action="admin_passwordprocessing.php" method="post">
                              <div class="row">
                                  <div class="col-md-12">
                                      <div class="form-group">
                                <label>New Password*</label> <input type="password"
                                    name="password" pattern=".{8,}"  class="form-control border-input"
                                    title="Password must be 8 characters or more" required /></div>
                                <input type="hidden" name="adminID"
                                    value="<?php echo  $adminID ?>">
                                  </div></div>


                                <div class="text-center">
                                  <button type="submit" class="btn btn-info btn-fill btn-wd" name="passwordupdate">
                                  Change Password
                                </div>
                                <div class="clearfix"></div>
                            </form>

              </div>
          </div>
      </div>
