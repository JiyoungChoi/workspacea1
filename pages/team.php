<?php 
$page = "about";
include '../include/header.php';
include '../include/connect.php';
?>

<div Style="background-image: url(../images/about-bg.jpg);  background-size:cover;">
    <div class="container" style="background-color: white; color: black; padding: 2%; height:100%; width:65%;">
        <?php
 
?>
        <h2 style="color:black;">Our Team</h2>
        <hr>
    <?php 
       
       $sql= mysqli_query($con, "SELECT * FROM team_members");
        while ($row= mysqli_fetch_array($sql)){
            
            echo'
            <div class="imgcontainerteam">
            <div class="overlayteam">
                <div class="textteam">More about me...</div>
            </div>
            <a href="team_members.php?$user_id='.$row['user_id'].'"><img class="about-team"'; 
            if($row['profile_picture'] != ""){ 
                echo 'src="../images/team/'.$row['profile_picture'].'"';
                    }else{
                        echo 'src="../images/team/no-profile-pic.jpg"';
                    }
                 echo   'alt="'.$row['name'].'"></a>
                 
                 <p> Name: '.$row['name'].'</p><p> Role: '.$row['role'].'</p>
        </div>
        ';
        }
        
        ?>
    </div>
</div>
<?php 

include '../include/footer.php';
?>