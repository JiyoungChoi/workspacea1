function validateregister() {
    var emailre = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var passwordre = /^([a-zA-Z0-9]{8,15})$/;
    var username = /^[a-zA-Z0-9_-]{3,15}$/;
    if (textfieldvalidator(document.getElementById("username"), username, "Invalid username format should be within 3 and 15 characters excluding special characters")) {
        if (textfieldvalidator(document.getElementById("email"), emailre, "Email should follow the standard format: example@example.com")) {
            if (textfieldvalidator(document.getElementById("password"), passwordre, "Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.")) {
                if (textfieldvalidator(document.getElementById("confirm_password"), passwordre, "Passwords must contain at least six characters, including uppercase, lowercase letters and numbers.")) {
                    if (checkmatch(document.getElementById("password").value, document.getElementById("confirm_password").value, "Oops... your passwords do not match")) {}
                }
            }
        }
    }
}

function textfieldvalidator(field_name, regex, message) {
    if (!field_name.value.match(regex)) {
        alert(message);
        field_name.focus();
        return false;
    } else {
        return true;
    }
}

function checkmatch(field_name1, field_name2, message) {
    if (field_name1 != field_name2) {
        alert(message);
        field_name1.focus();
        return false;
    } else {
        return true;
    }
}

function show() {
    var pass = document.getElementById("password");
    var confpass = document.getElementById("confirm_password");
    pass.setAttribute('type', 'text');
    confpass.setAttribute('type', 'text');
}

function hide() {
    var pass = document.getElementById("password");
    var confpass = document.getElementById("confirm_password");
    pass.setAttribute('type', 'password');
    confpass.setAttribute('type', 'password');
}

var pwShown = 0;

function viewpass() {
    if (pwShown == 0) {
        pwShown = 1;
        show();
    } else {
        pwShown = 0;
        hide();
    }
}