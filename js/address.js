
$(document).ready(function(){
  var map;
  var address = document.getElementById("address").innerHTML;

  function drawMap(pos){

      var mapOptions = {
          center: pos,
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
      var marker = new google.maps.Marker({
          position: pos,
          title: 'Initial Address',
          map: map
          // contents -> Insert it!! how?
      });
      // google.maps.event.addListener(marker, 'dragend', function() {
      //   // Change coodinates into 'Address'
      //   geocodePosition(marker.getPosition());
      // });
  }

  function getLocation(callback,address){
    //pos = document.getElementById('search').value;
    //var address = pos;
    //alert("say0");
    //await sleep(2000);
    var latitude,longitude;
    //var points=[];
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address: address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            //alert("address");
           //  for(var i=0;i<results.length;i++){
               latitude = results[0].geometry.location.lat();
               longitude = results[0].geometry.location.lng();
           //    //points.push( new google.maps.LatLng(latitude, longitude) );
           //  }
            //alert("here 1");
            //checkVar = checkVar+10;
            //callback(points);
            var loc = new google.maps.LatLng(latitude, longitude);
            //alert(loc);
            callback(loc);
        }
    });
  }
  //alert(address);
  getLocation(drawMap,address);
});
